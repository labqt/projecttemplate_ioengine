﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="utf.calculate.project.code.coverage" Type="Bool">false</Property>
	<Property Name="utf.create.arraybrackets" Type="Str">[]</Property>
	<Property Name="utf.create.arraythreshold" Type="UInt">100</Property>
	<Property Name="utf.create.captureinputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.captureoutputvalues" Type="Bool">true</Property>
	<Property Name="utf.create.codecoverage.flag" Type="Bool">false</Property>
	<Property Name="utf.create.codecoverage.value" Type="UInt">100</Property>
	<Property Name="utf.create.editor.flag" Type="Bool">false</Property>
	<Property Name="utf.create.editor.path" Type="Path"></Property>
	<Property Name="utf.create.nameseparator" Type="Str">/</Property>
	<Property Name="utf.create.precision" Type="UInt">6</Property>
	<Property Name="utf.create.repetitions" Type="UInt">1</Property>
	<Property Name="utf.create.testpath.flag" Type="Bool">false</Property>
	<Property Name="utf.create.testpath.path" Type="Path"></Property>
	<Property Name="utf.create.timeout.flag" Type="Bool">false</Property>
	<Property Name="utf.create.timeout.value" Type="UInt">0</Property>
	<Property Name="utf.create.type" Type="UInt">0</Property>
	<Property Name="utf.enable.RT.VI.server" Type="Bool">false</Property>
	<Property Name="utf.passwords" Type="Bin">%1#!#!!!!!)!%%!Q`````Q:4&gt;(*J&lt;G=!!":!1!!"`````Q!!#6"B=X.X&lt;X*E=Q!"!!%!!!!!!!!!!!</Property>
	<Property Name="utf.report.atml.create" Type="Bool">false</Property>
	<Property Name="utf.report.atml.path" Type="Path">ATML report.xml</Property>
	<Property Name="utf.report.atml.view" Type="Bool">false</Property>
	<Property Name="utf.report.details.errors" Type="Bool">false</Property>
	<Property Name="utf.report.details.failed" Type="Bool">false</Property>
	<Property Name="utf.report.details.passed" Type="Bool">false</Property>
	<Property Name="utf.report.errors" Type="Bool">true</Property>
	<Property Name="utf.report.failed" Type="Bool">true</Property>
	<Property Name="utf.report.html.create" Type="Bool">false</Property>
	<Property Name="utf.report.html.path" Type="Path">HTML report.html</Property>
	<Property Name="utf.report.html.view" Type="Bool">false</Property>
	<Property Name="utf.report.passed" Type="Bool">true</Property>
	<Property Name="utf.report.skipped" Type="Bool">true</Property>
	<Property Name="utf.report.sortby" Type="UInt">1</Property>
	<Property Name="utf.report.stylesheet.flag" Type="Bool">false</Property>
	<Property Name="utf.report.stylesheet.path" Type="Path"></Property>
	<Property Name="utf.report.summary" Type="Bool">true</Property>
	<Property Name="utf.report.txt.create" Type="Bool">false</Property>
	<Property Name="utf.report.txt.path" Type="Path">ASCII report.txt</Property>
	<Property Name="utf.report.txt.view" Type="Bool">false</Property>
	<Property Name="utf.run.changed.days" Type="UInt">1</Property>
	<Property Name="utf.run.changed.outdated" Type="Bool">false</Property>
	<Property Name="utf.run.changed.timestamp" Type="Bin">%1#!#!!!!!%!%%"5!!9*2'&amp;U:3^U;7VF!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	<Property Name="utf.run.days.flag" Type="Bool">false</Property>
	<Property Name="utf.run.includevicallers" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.overwrite" Type="Bool">false</Property>
	<Property Name="utf.run.logfile.path" Type="Path">test execution log.txt</Property>
	<Property Name="utf.run.modified.last.run.flag" Type="Bool">true</Property>
	<Property Name="utf.run.priority.flag" Type="Bool">false</Property>
	<Property Name="utf.run.priority.value" Type="UInt">5</Property>
	<Property Name="utf.run.statusfile.flag" Type="Bool">false</Property>
	<Property Name="utf.run.statusfile.path" Type="Path">test status log.txt</Property>
	<Property Name="utf.run.timestamp.flag" Type="Bool">false</Property>
	<Property Name="varPersistentID:{01C8CEC4-01F7-405D-B9FF-C24D24A20937}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/1 AnchorMotorSetSpeed_101</Property>
	<Property Name="varPersistentID:{036A23AB-1E75-4062-A4F0-02D4582CF607}" Type="Ref">/ControlSystem/VariablesSequence/UserOutputs.lvlib/TimeStable_101</Property>
	<Property Name="varPersistentID:{065C8A1B-4889-4C6F-A7F2-128E62DA928F}" Type="Ref">/ControlSystem/VariablesSequence/SequenceData.lvlib/StateTime_101</Property>
	<Property Name="varPersistentID:{09A0179F-E1AC-4C71-A4CB-04C96A831D5C}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/51 NC_126</Property>
	<Property Name="varPersistentID:{0B7F0F0E-CCC9-4C97-A924-53ABC52E7992}" Type="Ref">/ControlSystem/VariablesSequence/Setpoints.lvlib/SpPressure_102</Property>
	<Property Name="varPersistentID:{0B9A3F4E-002C-4983-8280-FA2D4E50A2AD}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/45 NC_162</Property>
	<Property Name="varPersistentID:{0C2A4DD1-4A03-4AFA-89BB-DF189210F902}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/24 NC_157</Property>
	<Property Name="varPersistentID:{0C924081-C213-49D0-B701-105877AE93E1}" Type="Ref">/ControlSystem/VariablesSequence/SequenceData.lvlib/CurrentState_101</Property>
	<Property Name="varPersistentID:{0D35D431-BF1D-461F-8D1F-355F7F4935E1}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/34 CoolingFan_101</Property>
	<Property Name="varPersistentID:{0D595B9F-B207-4DA6-BEF0-9D051E0AD211}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/13 NC_103</Property>
	<Property Name="varPersistentID:{1318A3C1-131D-425D-B0E8-7DA83EE4D361}" Type="Ref">/ControlSystem/VariablesSequence/Criterias.lvlib/PressureCriteria_101</Property>
	<Property Name="varPersistentID:{17602FC4-80C7-4497-9777-13E90BD80690}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/44 NC_161</Property>
	<Property Name="varPersistentID:{1E1D9FB3-4626-4321-B04B-0C366431F89B}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/18 NC_152</Property>
	<Property Name="varPersistentID:{1EB5A4F5-138C-47E1-B221-EA22B6424EF5}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/16 NC_106</Property>
	<Property Name="varPersistentID:{216DD340-B656-4480-A0EA-C746B8F73391}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/8 NC_144</Property>
	<Property Name="varPersistentID:{231BE42D-D67D-4082-A11F-766FA9ACC0FF}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/8 NC_144</Property>
	<Property Name="varPersistentID:{23BBE060-6247-4077-B7E6-3D9E34B58EDA}" Type="Ref">/ControlSystem/VariablesSequence/SupportVariables.lvlib/ZZWatchdogPet_101</Property>
	<Property Name="varPersistentID:{256494BD-02DD-4D66-B83F-287C75139014}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/30 NC_120</Property>
	<Property Name="varPersistentID:{25ACCE9D-5D5F-4C1D-86FF-691464F58E32}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/25 NC_158</Property>
	<Property Name="varPersistentID:{28C935E3-DCD4-47B8-802D-AF9FB9C3A560}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/10 BilgePump_103</Property>
	<Property Name="varPersistentID:{28FB089D-5C7E-4CDB-87A5-51B9A3AEBE0A}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/9 WallasHeaterStatus_101</Property>
	<Property Name="varPersistentID:{2B6ED71F-E865-4D27-9808-4D2AC81A21F1}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/0 GeneratorSetSpeed_101</Property>
	<Property Name="varPersistentID:{2C16D271-48CE-4894-8155-E0A7999DB5FE}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/21 DCDC540To24V_101</Property>
	<Property Name="varPersistentID:{2C73171E-B60C-4A65-90AA-EF9401004E67}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/7 TransmissionHouseTemperature_101</Property>
	<Property Name="varPersistentID:{2CA71FAD-3D8F-4810-804C-5ECB08D2A9D2}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/4 IntermediateShaft2Torque_101</Property>
	<Property Name="varPersistentID:{2E3D17A6-3638-470E-9EBD-41039E4C5C8F}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/57 NC_132</Property>
	<Property Name="varPersistentID:{2F800B28-6FE4-4DA1-A7BF-A69E718C4B32}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/29 BilgePump_106</Property>
	<Property Name="varPersistentID:{2FABF483-334C-48B0-884B-BCF888B68CFA}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/35 AnchorMotorBrake_101</Property>
	<Property Name="varPersistentID:{301FC7BC-B8D5-4FE3-89C7-DFD82CE74771}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/1 GeneratorForward_101</Property>
	<Property Name="varPersistentID:{3125611E-B231-420D-82D7-D86877B37875}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/64 PumpPosition_101</Property>
	<Property Name="varPersistentID:{3127584D-4827-4575-B1B5-AF1559C783A3}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/58 NC_133</Property>
	<Property Name="varPersistentID:{312E1BAF-5E4C-4E5C-80DA-755C7002D9C2}" Type="Ref">/ControlSystem/VariablesSequence/RegulatorParameters.lvlib/RegulatorMode_103</Property>
	<Property Name="varPersistentID:{315177C0-07B3-4786-8CC4-547B830DF7AA}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/5 NC_153</Property>
	<Property Name="varPersistentID:{3500B02B-6AC3-4EDF-A9EF-51C2CD61160D}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/5 RackForce_101</Property>
	<Property Name="varPersistentID:{375BCED9-70BA-4E28-9206-A4846407CB72}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/61 NC_136</Property>
	<Property Name="varPersistentID:{376B9A86-F698-4874-8DC9-5BE0CEE54F7D}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/7 Clutch_101</Property>
	<Property Name="varPersistentID:{40145295-4BB5-4EF2-AB01-5EBCEB19BF9C}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/34 FrequencyInverterGeneratorFAULTY_101</Property>
	<Property Name="varPersistentID:{42AEE38B-D518-4E93-922F-163887597293}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/32 CirculationWaterPump_101</Property>
	<Property Name="varPersistentID:{43491BF0-5256-4D82-AA36-2353D7C2D875}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/46 NC_163</Property>
	<Property Name="varPersistentID:{4711EFBA-7B1A-40CD-9984-5E4150DF66C8}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/42 NC_159</Property>
	<Property Name="varPersistentID:{4739CFB0-C4D7-4AF8-80FD-6B418D086192}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/21 NC_153</Property>
	<Property Name="varPersistentID:{47C237E2-075B-410F-B0CB-8A4AC105BABC}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/4 AnchorMotorForward_101</Property>
	<Property Name="varPersistentID:{48ADE33C-EF3E-41D5-9045-4246AE524265}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/8 BilgePump_101</Property>
	<Property Name="varPersistentID:{4AB35EAD-BFB1-44A4-8A39-041F193328AE}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/17 NC_107</Property>
	<Property Name="varPersistentID:{4B6F43B8-66B7-49C5-B04D-FF3D288BD4C4}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/19 AnchorMotorStart_101</Property>
	<Property Name="varPersistentID:{4BAFA012-0162-4EC2-A8A7-4D713FEDAAF9}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/0 GeneratorStart_101</Property>
	<Property Name="varPersistentID:{4CC874A7-838B-4AF7-A180-635180F8E09F}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/2 AnchorMotorSetTorque_101</Property>
	<Property Name="varPersistentID:{4E1217F8-D5D3-4957-90AD-75787CA48862}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/39 NC_122</Property>
	<Property Name="varPersistentID:{4F63BBA1-6846-47BE-B740-76BF3E6D7E1C}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/60 NC_135</Property>
	<Property Name="varPersistentID:{4FA8C765-B866-47FE-8C0C-BCA10CCBE1C7}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/48 BatteryChargerFAULTY_101</Property>
	<Property Name="varPersistentID:{50C1413F-C7DE-40D4-B494-AE2C83176B06}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/13 NC_149</Property>
	<Property Name="varPersistentID:{516CD2BE-BD43-4178-870F-5BFF7A410275}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/27 NC_160</Property>
	<Property Name="varPersistentID:{52331101-B7F8-4A4C-A36F-FB85855CD9F7}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/31 NC_121</Property>
	<Property Name="varPersistentID:{544F701D-D79E-401F-B453-529EF57C34CC}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/14 NC_104</Property>
	<Property Name="varPersistentID:{546160CC-6768-4A89-BB8B-A9BFF52D7E30}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/28 NC_161</Property>
	<Property Name="varPersistentID:{54BDEC48-79DA-499D-A339-C6EB3C19429E}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/19 AnchorMotorBrake_101</Property>
	<Property Name="varPersistentID:{5538817D-ABD8-42D6-991A-1CA908D4E3B8}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/4 NC_140</Property>
	<Property Name="varPersistentID:{554C4C2D-DC0E-4AC1-9F92-4CDAD9823247}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/12 NC_148</Property>
	<Property Name="varPersistentID:{559E3834-9D5E-42B2-9E6D-B7842BC3E3A3}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/9 NC_145</Property>
	<Property Name="varPersistentID:{5AAE96CF-100F-4935-B925-F8E7FC68F8E2}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/63 NC_138</Property>
	<Property Name="varPersistentID:{5B3E12B9-A233-4422-AD3A-295A4E0CDEF5}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/14 NC_154</Property>
	<Property Name="varPersistentID:{5B67CC51-DF42-4242-A6CE-B6203C28A42B}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/31 NC_164</Property>
	<Property Name="varPersistentID:{5B8121B8-DA1B-4294-B0E5-F830170162EC}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/1 GeneratorPower_101</Property>
	<Property Name="varPersistentID:{5BD6E2EB-5845-4CBE-BEA6-BF77748E9B23}" Type="Ref">/ControlSystem/VariablesSequence/Setpoints.lvlib/SpLevel_101</Property>
	<Property Name="varPersistentID:{5BDCEE16-50A6-4BA7-83D6-E22A9606C428}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/20 NC_155</Property>
	<Property Name="varPersistentID:{5C24ED56-E1C8-420F-B209-EA65CE369487}" Type="Ref">/ControlSystem/VariablesSequence/UserOutputs.lvlib/PowerTolerance_101</Property>
	<Property Name="varPersistentID:{5CD54B9A-D763-40A5-8BF8-B109363CE47A}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/17 WallasHeaterStart_101</Property>
	<Property Name="varPersistentID:{5EC0FC49-C8FF-4523-B3AD-CFA37F335D30}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/42 WaterLevel_103</Property>
	<Property Name="varPersistentID:{6266713F-EF35-48CB-B7EF-2974E1C4C063}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/14 NC_150</Property>
	<Property Name="varPersistentID:{62E181A9-37CF-4C0B-87C9-C52549776861}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/15 NC_151</Property>
	<Property Name="varPersistentID:{6BB29B14-B5FB-4FC6-949C-8AABD59A25A6}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/44 WaterLevel_105</Property>
	<Property Name="varPersistentID:{6BE5E3A3-BCC8-43E1-ABE5-B4A21BB9B3ED}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/24 BilgePump_101</Property>
	<Property Name="varPersistentID:{6CA622D1-F3AB-41B9-9500-32115D362D71}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/47 DCDCConverterFAULTY_101</Property>
	<Property Name="varPersistentID:{6CF3A8DC-2829-4296-9516-91551524064D}" Type="Ref">/ControlSystem/VariablesSequence/Setpoints.lvlib/SpPower_101</Property>
	<Property Name="varPersistentID:{6D425A75-EF51-49D9-BCDE-E1133652BBE7}" Type="Ref">/ControlSystem/VariablesSequence/Setpoints.lvlib/SpPower_102</Property>
	<Property Name="varPersistentID:{6D67A050-00B2-45D7-A250-78CC81DF4D4C}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/65 RackPosition_101</Property>
	<Property Name="varPersistentID:{6DF47A6D-01AD-4DB8-9337-5AFD0E52039A}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/1 AnchorMotorSetSpeed_101</Property>
	<Property Name="varPersistentID:{70AF4455-404B-4971-83F9-1014F5A405D3}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/30 NC_154</Property>
	<Property Name="varPersistentID:{70E36E40-13FA-4A1D-A442-C1EB1E3A2FD2}" Type="Ref">/ControlSystem/VariablesSequence/Criterias.lvlib/TimeStableCriteria_101</Property>
	<Property Name="varPersistentID:{71479D6F-9490-42EE-92FB-DEE2FD40C2EE}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/38 BatteryChargerDCDC540VTo24V_101</Property>
	<Property Name="varPersistentID:{7379EA80-C8E8-4BB3-8E61-FB1131681DE9}" Type="Ref">/ControlSystem/VariablesSequence/SupportVariables.lvlib/StableTimer_101</Property>
	<Property Name="varPersistentID:{755DEEA9-8884-4A64-9BA4-4B13DADC49A9}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/3 AnchorMotorStart_101</Property>
	<Property Name="varPersistentID:{762F57BA-4940-494D-84AA-09A282ED6BC4}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/7 NC_143</Property>
	<Property Name="varPersistentID:{7A494957-BE9C-4BB7-B04A-5BBFE23B40CA}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/28 BilgePump_105</Property>
	<Property Name="varPersistentID:{7C64E345-D06E-4075-8DFD-102B7FAB5BC3}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/16 GeneratorStart_101</Property>
	<Property Name="varPersistentID:{7DBB6F3A-8C64-49FA-A7AA-58D469240D8D}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/45 WaterLevel_106</Property>
	<Property Name="varPersistentID:{7DCAC589-1F8E-4776-939F-8F636742E103}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/43 WaterLevel_104</Property>
	<Property Name="varPersistentID:{7F058F99-B4B2-4B64-99C3-D647DCB1D226}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/40 NC_157</Property>
	<Property Name="varPersistentID:{7F799B34-6EE7-4276-9240-D212D6BAB282}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/10 NC_146</Property>
	<Property Name="varPersistentID:{809C82E5-0436-4BDF-AF7E-516CA15557F5}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/0 GeneratorSpeed_101</Property>
	<Property Name="varPersistentID:{83CF72E6-1673-4FCA-8FBE-3B149A4FE305}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/11 BilgePump_104</Property>
	<Property Name="varPersistentID:{842B7E5E-D387-4216-BCBC-2BAB84526F2B}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/50 NC_125</Property>
	<Property Name="varPersistentID:{87C73524-4858-4B80-83EC-4B11A40ED7D1}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/15 NC_151</Property>
	<Property Name="varPersistentID:{883BCA87-AE79-4444-A540-1344F74E82C7}" Type="Ref">/ControlSystem/VariablesSequence/Setpoints.lvlib/SpPressure_101</Property>
	<Property Name="varPersistentID:{88522F98-0E6B-42CE-AB16-839FD773E753}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/40 WaterLevel_101</Property>
	<Property Name="varPersistentID:{88C7681C-C394-441B-9901-2BAB324DE046}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/19 NC_109</Property>
	<Property Name="varPersistentID:{8947E7E5-F118-4118-BCD4-0F43510D2B98}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/17 GeneratorForward_101</Property>
	<Property Name="varPersistentID:{89C0004F-806B-4372-BFB2-B0EAD73EAA4B}" Type="Ref">/ControlSystem/VariablesSequence/UserOutputs.lvlib/PressureTolerance_102</Property>
	<Property Name="varPersistentID:{8B43E827-5C8A-448E-B886-9719E59D607B}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/3 AnchorMotorPower_101</Property>
	<Property Name="varPersistentID:{8C9EFED9-9270-40F0-95FB-8A6DE654385F}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/24 NC_114</Property>
	<Property Name="varPersistentID:{8CBE175F-C7F0-418A-9527-57909AA7C748}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/22 ElectroHydraulicBrake_101</Property>
	<Property Name="varPersistentID:{8D0D4C9E-7A83-4C30-9761-4BA851769703}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/9 BilgePump_102</Property>
	<Property Name="varPersistentID:{8F2CCF13-628F-41B3-8512-2655FA45F02E}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/12 NC_148</Property>
	<Property Name="varPersistentID:{8FBC237F-E275-48A6-A68B-BBA7C846A462}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/6 EnclosureHumidity_101</Property>
	<Property Name="varPersistentID:{914B73C7-06C7-4238-9CF6-04A837213FD6}" Type="Ref">/ControlSystem/VariablesSequence/RegulatorParameters.lvlib/RegulatorMode_102</Property>
	<Property Name="varPersistentID:{93EAF8F1-03F4-44B8-B1C7-2E38AD83C799}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/29 NC_162</Property>
	<Property Name="varPersistentID:{949235FF-1302-48D7-A742-2E1705008172}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/55 NC_130</Property>
	<Property Name="varPersistentID:{94B5BA08-A94C-455A-A318-C39795FF1C8D}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/26 BilgePump_103</Property>
	<Property Name="varPersistentID:{951095C1-FBF3-413C-BE67-C2D8B484A442}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/3 DCDCConverterRamp_101</Property>
	<Property Name="varPersistentID:{968FCB55-AE5D-4E6A-A86C-C9650FFAE428}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/15 SeaWaterPump_101</Property>
	<Property Name="varPersistentID:{9C7C82C0-816A-4D37-A46D-BE3AC197D7D1}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/0 GeneratorSetSpeed_101</Property>
	<Property Name="varPersistentID:{9CA23F5F-AE95-4A7E-9CCB-F7DDE46A5314}" Type="Ref">/ControlSystem/VariablesSequence/Criterias.lvlib/PressureCriteria_102</Property>
	<Property Name="varPersistentID:{9F3A2954-4692-4D83-BB37-3ABFC895D697}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/13 BilgePump_106</Property>
	<Property Name="varPersistentID:{A00C6529-C677-4C98-88AE-5BF4F8870FA5}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/6 ElectroHydraulicBrake_101</Property>
	<Property Name="varPersistentID:{A1EAE197-C462-4926-8604-9846DB2DDF1F}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/11 NC_101</Property>
	<Property Name="varPersistentID:{A1F41C88-EBBB-45FE-B038-4240EAC705C2}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/15 NC_105</Property>
	<Property Name="varPersistentID:{A3FD9E04-BEE3-47D1-9F2A-5C4EC0CC471F}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/28 NC_118</Property>
	<Property Name="varPersistentID:{A434161E-B6D7-4066-B488-43BDE2E807FB}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/62 NC_137</Property>
	<Property Name="varPersistentID:{A465F504-C82F-4BB8-96FE-6367C317691B}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/33 FrequencyInverterGeneratorRUNNING_101</Property>
	<Property Name="varPersistentID:{A4887532-E62E-43D6-8C7B-C9A4ECDC03CE}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/3 DCDCConverterRamp_101</Property>
	<Property Name="varPersistentID:{A72F98ED-35E6-4110-891E-2CDA16EAAE8B}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/52 NC_127</Property>
	<Property Name="varPersistentID:{A73468F3-FA94-443E-9FA6-7AA5989A22A9}" Type="Ref">/ControlSystem/VariablesSequence/Criterias.lvlib/LevelCriteria_101</Property>
	<Property Name="varPersistentID:{A7FA33A9-48E3-4D7D-8324-6BB0B3DDF55C}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/6 NC_142</Property>
	<Property Name="varPersistentID:{A8BFD8F2-07F5-44BD-B2C7-FF303904EEDF}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/10 Depth_101</Property>
	<Property Name="varPersistentID:{AA83F7B1-2D8A-415A-8919-8D9C06960DE6}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/20 NC_110</Property>
	<Property Name="varPersistentID:{ABCCF63F-7565-4209-8A4F-897404FA0A99}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/29 NC_119</Property>
	<Property Name="varPersistentID:{AD356E40-C061-4333-882F-4809BE700182}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/2 AnchorMotorSetTorque_101</Property>
	<Property Name="varPersistentID:{AE6E0B7E-8FFC-479D-B913-21A7AC248D1A}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/25 BilgePump_102</Property>
	<Property Name="varPersistentID:{AF6D571C-FC1A-4714-B2B1-320B3DB305C9}" Type="Ref">/ControlSystem/VariablesSequence/UserOutputs.lvlib/Timeout_101</Property>
	<Property Name="varPersistentID:{B01A8A5A-19AD-4025-A2B0-33A6B909F8BB}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/56 NC_131</Property>
	<Property Name="varPersistentID:{B02A2100-8027-4320-80E0-A4E820B7083F}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/33 WallasHeaterStart_101</Property>
	<Property Name="varPersistentID:{B030E72F-06C4-4A3A-9E46-19000E787612}" Type="Ref">/ControlSystem/VariablesSequence/SequenceData.lvlib/TotalSteps_101</Property>
	<Property Name="varPersistentID:{B0508F58-06D2-4633-9089-3EF148786D46}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/18 NC_108</Property>
	<Property Name="varPersistentID:{B2B01A7B-1F8B-475E-95CD-4D2A2CA36448}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/30 NC_163</Property>
	<Property Name="varPersistentID:{B3F41288-A0BF-485F-B96E-B42E4B7C9C04}" Type="Ref">/ControlSystem/VariablesSequence/Criterias.lvlib/PowerCriteria_101</Property>
	<Property Name="varPersistentID:{B6AA84A9-C7DE-4073-85C6-72C1FB2729EF}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/4 NC_140</Property>
	<Property Name="varPersistentID:{B6D8FBB5-DA9E-47ED-8C17-03D9D017A13D}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/49 NC_124</Property>
	<Property Name="varPersistentID:{B806F8AA-9637-40A0-8769-64B16E1C712F}" Type="Ref">/ControlSystem/VariablesSequence/Criterias.lvlib/TimeoutCriteria_101</Property>
	<Property Name="varPersistentID:{BA70F43A-D893-4277-B46F-D4ED3D578CC5}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/22 NC_112</Property>
	<Property Name="varPersistentID:{BAD00012-06AA-47FA-9351-8C806AC6B46F}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/12 NC_102</Property>
	<Property Name="varPersistentID:{BAD823CA-0239-4E7D-91D1-0D59FF55AA5A}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/7 NC_143</Property>
	<Property Name="varPersistentID:{BD395DF5-FA6C-4D42-9F4C-7D45E4967CC3}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/25 NC_115</Property>
	<Property Name="varPersistentID:{BD950F03-8AC6-4007-AB54-BC92262C448E}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/26 NC_159</Property>
	<Property Name="varPersistentID:{BF30E723-9AD9-45C0-9F83-A3BB2E39633C}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/36 FrequencyInverterAnchorMotorRUNNING_101</Property>
	<Property Name="varPersistentID:{C03CBBF2-3897-42DF-8E19-4021A2D3EEC2}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/5 NC_141</Property>
	<Property Name="varPersistentID:{C12EC1E2-8424-4729-A969-D5FD3DEB8CA4}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/27 NC_117</Property>
	<Property Name="varPersistentID:{C24E7923-9A27-4333-8012-E652D8EC090D}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/2 NC_152</Property>
	<Property Name="varPersistentID:{C352D065-2993-424E-8773-0DD7E64C3AB9}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/12 BilgePump_105</Property>
	<Property Name="varPersistentID:{C3804417-8DE2-4553-9AFF-C699906F5A10}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/47 NC_164</Property>
	<Property Name="varPersistentID:{C44E1260-019C-4A67-9C7C-452029A0E6B9}" Type="Ref">/ControlSystem/VariablesSequence/SequenceLogic.lvlib/NextState_101</Property>
	<Property Name="varPersistentID:{C73D8195-7B32-4346-9D8F-B7CC54431B09}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/27 BilgePump_104</Property>
	<Property Name="varPersistentID:{C893FAFF-1455-46F2-82B7-04DBF69A41A1}" Type="Ref">/ControlSystem/VariablesSequence/SequenceLogic.lvlib/Criteria_101</Property>
	<Property Name="varPersistentID:{C947F667-EA63-4435-A98D-DF51F800DE91}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/9 NC_145</Property>
	<Property Name="varPersistentID:{CD22CBF9-DB88-48BD-8A5B-AE62B653E211}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/59 NC_134</Property>
	<Property Name="varPersistentID:{CE443BE8-47CC-4E6D-9105-4A1B695E4616}" Type="Ref">/ControlSystem/VariablesSequence/UserOutputs.lvlib/NumberOfDuts_101</Property>
	<Property Name="varPersistentID:{CFB3188A-7A51-45E7-86D2-632E4E8FE088}" Type="Ref">/ControlSystem/VariablesSequence/SequenceData.lvlib/CurrentStep_101</Property>
	<Property Name="varPersistentID:{D61235F5-A9FE-4B14-8533-D2F8B2776BFD}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/37 FrequencyInverterAnchorMotorFAULTY_101</Property>
	<Property Name="varPersistentID:{D634AA36-2579-4F6F-85FD-4153980D6B35}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/35 FrequencyInverterAnchorMotorREADY_101</Property>
	<Property Name="varPersistentID:{D6D3815B-B756-4E63-93F8-872E599584F8}" Type="Ref">/ControlSystem/VariablesSequence/Setpoints.lvlib/SpPower_103</Property>
	<Property Name="varPersistentID:{D6E64861-0F09-43C3-9183-69532699B3DC}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/2 AnchorMotorSpeed_101</Property>
	<Property Name="varPersistentID:{D711B4EB-0CCC-449B-BDD8-4C48EC666781}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/23 NC_113</Property>
	<Property Name="varPersistentID:{D78DEB02-2434-49A1-B5D3-33ADA328CE85}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/23 NC_156</Property>
	<Property Name="varPersistentID:{DD648E57-CDAE-4652-98C4-06CAFEA1682C}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/8 BatteryGroupVoltage_101</Property>
	<Property Name="varPersistentID:{DF4C635A-891D-4AF5-96C1-E0920AE98C43}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/32 FrequencyInverterGeneratorREADY_101</Property>
	<Property Name="varPersistentID:{DF54735F-CBFF-4502-90A1-9B0956C2439A}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/22 BatteryChargerDCDC540Vto24V</Property>
	<Property Name="varPersistentID:{E182E767-B600-4909-B344-7D89E315D2A6}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/31 SeaWaterPump_101</Property>
	<Property Name="varPersistentID:{E2A4B2C9-D04F-43F9-8F45-10E2D3468F51}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/13 NC_149</Property>
	<Property Name="varPersistentID:{E37597A8-A413-4312-9471-7C009EF04268}" Type="Ref">/ControlSystem/VariablesSequence/SequenceLogic.lvlib/Pause_101</Property>
	<Property Name="varPersistentID:{E3CD5C2D-FC46-4B55-B706-9BC4526E4F58}" Type="Ref">/ControlSystem/VariablesSequence/UserOutputs.lvlib/DutType_101</Property>
	<Property Name="varPersistentID:{E5B79316-D1C9-49A5-94A8-53728B85E735}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/46 NC_123</Property>
	<Property Name="varPersistentID:{E7045818-5789-48B4-8466-5996C738519E}" Type="Ref">/ControlSystem/VariablesSequence/UserOutputs.lvlib/PressureTolerance_101</Property>
	<Property Name="varPersistentID:{E8212198-A21E-4E0C-B0A6-0D7CF2188A9D}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/43 NC_160</Property>
	<Property Name="varPersistentID:{E8608362-1BB6-4440-B76C-41124342D05E}" Type="Ref">/ControlSystem/VariablesSequence/UserOutputs.lvlib/LevelTolerance_101</Property>
	<Property Name="varPersistentID:{E887F242-A4A7-4A58-A1B0-348D1E0A477F}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/6 NC_142</Property>
	<Property Name="varPersistentID:{E97CA6CD-73C5-469C-AC74-49F607059563}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/41 NC_158</Property>
	<Property Name="varPersistentID:{EA874B48-66E8-4451-AA94-27C9DB0F9579}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/5 NC_141</Property>
	<Property Name="varPersistentID:{EAAB3AF8-6B24-4E0E-B1F3-B08594C915E7}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/23 Clutch_101</Property>
	<Property Name="varPersistentID:{EB0BF731-2C73-4222-9CBB-605DF6972883}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/39 NC_156</Property>
	<Property Name="varPersistentID:{EE8F5FB6-1409-4EE3-B35E-FC1A8135D264}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/16 CirculationWaterPump_101</Property>
	<Property Name="varPersistentID:{F18C1906-555A-451B-B5BB-31BD9FC45146}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/10 NC_146</Property>
	<Property Name="varPersistentID:{F19FFE4D-E2B2-4CD4-9253-80FE5A6CBDBD}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/53 NC_128</Property>
	<Property Name="varPersistentID:{F537F6D1-F182-4364-8C07-B1403F06331A}" Type="Ref">/ControlSystem/Chassis/cRIOAnalogOutput/11 NC_147</Property>
	<Property Name="varPersistentID:{F621E98C-7476-474B-987D-F0CE50B133A2}" Type="Ref">/ControlSystem/VariablesSequence/RegulatorParameters.lvlib/RegulatorMode_101</Property>
	<Property Name="varPersistentID:{F690F2D9-E248-4896-BB84-AC371687CB5E}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/37 DCDC540VTo24V_101</Property>
	<Property Name="varPersistentID:{F6E83D84-55A9-4BCC-80FF-828B4A1821F4}" Type="Ref">/ControlSystem/Chassis/cRIODigitalOutput/18 CoolingFan_101</Property>
	<Property Name="varPersistentID:{F79533F6-3DDC-4C51-98D5-281B404720D7}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/14 NC_150</Property>
	<Property Name="varPersistentID:{F85544BD-9EC2-4C97-88AB-97387C8B2F15}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/54 NC_129</Property>
	<Property Name="varPersistentID:{F887A98B-BBAE-4930-85B8-33BE8922D9F5}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/41 WaterLevel_102</Property>
	<Property Name="varPersistentID:{F9EF7EFD-3437-4091-903A-8F1791E8B04A}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/21 NC_111</Property>
	<Property Name="varPersistentID:{FB02FCCA-6B59-4103-94C0-9E41F10BB3F8}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/26 NC_116</Property>
	<Property Name="varPersistentID:{FC0CFDE9-FB34-4620-8065-1B52A68C3AA6}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/20 AnchorMotorForward_101</Property>
	<Property Name="varPersistentID:{FCCBD2B3-6180-4A9E-885D-0724CE894019}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/11 NC_147</Property>
	<Property Name="varPersistentID:{FD615060-BB50-4F54-9EB4-96D572A355B8}" Type="Ref">/ControlSystem/OutputVariables/cRIOOutputs.lvlib/36 NC_155</Property>
	<Property Name="varPersistentID:{FDBCB019-310D-4F81-85EC-7653695B86E0}" Type="Ref">/ControlSystem/InputVariables/cRIOInputs.lvlib/38 EmergencyStop_101</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str"></Property>
		<Property Name="DisableAutoDeployVariables" Type="Bool">true</Property>
		<Property Name="IOScan.Faults" Type="Str">1.0,0;</Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">100000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">true</Property>
		<Property Name="mathScriptPath" Type="Str">\\psf\Home\Documents\LabVIEW Data</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.acl" Type="Str">0800000008000000</Property>
		<Property Name="server.tcp.enabled" Type="Bool">true</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str"></Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.access" Type="Str"></Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">true</Property>
		<Property Name="server.viscripting.showScriptingOperationsInEditor" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Dependencies" Type="Dependencies">
			<Property Name="NI.SortType" Type="Int">0</Property>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="ControlSystem" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">ControlSystem</Property>
		<Property Name="alias.value" Type="Str">192.168.1.11</Property>
		<Property Name="CCSymbols" Type="Str">OS,VxWorks;CPU,PowerPC;TARGET_TYPE,RT;</Property>
		<Property Name="crio.ControllerPID" Type="Str">7459</Property>
		<Property Name="crio.family" Type="Str">901x</Property>
		<Property Name="DisableAutoDeployVariables" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">20000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">20000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">2</Property>
		<Property Name="host.TargetOSID" Type="UInt">14</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str">1.0,0;</Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">100000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">true</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str"></Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">true</Property>
		<Property Name="target.server.viscripting.showScriptingOperationsInEditor" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str"># Web server configuration file.
# Generated by LabVIEW 11.0.1f2
# 2016-03-01 12:59:57
#
# Global Directives
#
NI.AddLVRouteVars
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
LimitWorkers 10
LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule
LoadModule niSslInitModule mod_nissl
LoadModule sslModule mod_ssl
Listen 8000
Listen 443
#
# Directives that apply to the default server
#
NI.ServerName LabVIEW
DocumentRoot "$LVSERVER_DOCROOT"
InactivityTimeout 60
SetConnector netConnector
AddHandler LVAuth
AddHandler LVRFP
AddHandler fileHandler ""
AddOutputFilter chunkFilter
DirectoryIndex index.htm
#
# Directives for VirtualHost SSL
#
&lt;VirtualHost *:443&gt;
NI.ServerName SSL
SSLEngine on
SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
SSLProtocol ALL -SSLV2
&lt;/VirtualHost&gt;
&lt;VirtualHost *:443&gt;
NI.ServerName SSL
SSLEngine on
SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
SSLProtocol ALL -SSLV2
&lt;/VirtualHost&gt;
&lt;VirtualHost *:443&gt;
NI.ServerName SSL
SSLEngine on
SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
SSLProtocol ALL -SSLV2
&lt;/VirtualHost&gt;
&lt;VirtualHost *:443&gt;
NI.ServerName SSL
SSLEngine on
SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
SSLProtocol ALL -SSLV2
&lt;/VirtualHost&gt;
&lt;VirtualHost *:443&gt;
NI.ServerName SSL
SSLEngine on
SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
SSLProtocol ALL -SSLV2
&lt;/VirtualHost&gt;
#
# Directives for VirtualHost SSL
#
&lt;VirtualHost *:443&gt;
NI.ServerName SSL
&lt;/VirtualHost&gt;
</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Property Name="TargetOSID" Type="Str">VxWorks-PPC603</Property>
		<Item Name="TestCode" Type="Folder" URL="../TestCode">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="InputVariables" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="cRIOInputs.lvlib" Type="Library" URL="../Variables/cRIOInputs.lvlib"/>
		</Item>
		<Item Name="OutputVariables" Type="Folder">
			<Item Name="cRIOOutputs.lvlib" Type="Library" URL="../Variables/cRIOOutputs.lvlib"/>
		</Item>
		<Item Name="VariablesSequence" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Criterias.lvlib" Type="Library" URL="../Variables/Criterias.lvlib"/>
			<Item Name="RegulatorParameters.lvlib" Type="Library" URL="../Variables/RegulatorParameters.lvlib"/>
			<Item Name="SequenceData.lvlib" Type="Library" URL="../Variables/SequenceData.lvlib"/>
			<Item Name="SequenceLogic.lvlib" Type="Library" URL="../Variables/SequenceLogic.lvlib"/>
			<Item Name="Setpoints.lvlib" Type="Library" URL="../Variables/Setpoints.lvlib"/>
			<Item Name="SupportVariables.lvlib" Type="Library" URL="../Variables/SupportVariables.lvlib"/>
			<Item Name="UserOutputs.lvlib" Type="Library" URL="../Variables/UserOutputs.lvlib"/>
		</Item>
		<Item Name="Misc" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="ConfigLocal.ini" Type="Document" URL="../Misc/ConfigLocal.ini"/>
			<Item Name="ReferenceInstruments.ini" Type="Document" URL="../Misc/ReferenceInstruments.ini"/>
			<Item Name="Application.ctl" Type="VI" URL="../Misc/Application.ctl"/>
			<Item Name="AlarmType.ctl" Type="VI" URL="../Misc/AlarmType.ctl"/>
			<Item Name="ReadCalibrationData.vi" Type="VI" URL="../Misc/ReadCalibrationData.vi"/>
			<Item Name="ReadConfigLocal.vi" Type="VI" URL="../Misc/ReadConfigLocal.vi"/>
		</Item>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">fpga</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9114</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="FPGA ControlSystem" Type="FPGA Target">
				<Property Name="AutoRun" Type="Bool">false</Property>
				<Property Name="configString.guid" Type="Str">{0073020B-2D9F-4450-8A6D-FB85A9636099}resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=bool{02D7301B-E6A1-4790-AC7E-B006C2CF5330}resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=bool{03B06DD0-C38E-40B6-B789-2E7364394881}resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=bool{047B03B7-47D4-4337-9691-DDE46857195C}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=bool{059C4215-D03A-439D-8C82-FFCB009DE0A2}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=bool{071122DE-3456-4D34-A2D3-28C6CD377950}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=bool{080F1513-66C2-421C-BFD9-567E9E5D3E1A}resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{0AE0D5EB-8DE3-4DDD-A2F1-7D02906FB4FC}resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8{0B0FD5A8-C8D0-4E34-AB64-10E9EDF189E7}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=bool{113EA66B-7BEB-4F5A-9BDC-A24733C2A8F1}resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=bool{120D309E-6AA7-429C-BAD9-42306571CCA2}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1640CC45-486E-42ED-A03C-88D12527AF0F}resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{19803E27-A2F2-4517-A7B0-73EDF239B4D6}resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1991A0AF-7D17-4493-B5B2-1DCED6DB502E}resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1F2A7A89-9342-457F-BF2A-C26D1A01A2E4}resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=bool{26CF9E8D-7B3D-4828-9ABC-788302291FBE}resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=bool{27335150-337C-458F-AEDF-BDF23C6820CC}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=bool{29555C81-E676-4708-B543-E8C48A518EB3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2AB3B169-20FC-4443-BE33-975E88F509B8}resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=bool{2E7911A3-ACDC-4265-A2CC-02DF0A0A590D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{33A4CA04-47E3-4DF7-BD69-5E12C1DBDD4C}resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=bool{35534149-09DE-4991-A46F-A239DB71862B}resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=bool{366B01FF-7B7A-4B4D-A8EF-A1D853A5965B}resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=bool{370EE507-4F8F-416A-A0BE-1D3395932C93}resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=bool{374BD079-EA0B-414D-8C4A-986E34C79C0E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{37990431-8C69-4CCA-AB20-4037E80322F0}resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=bool{3BD18584-A6E5-469E-8948-BF5DCDBCB8E3}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3BDAD3AA-D7D6-45FD-8782-47F37B3991F8}resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3C9EC768-98CD-43AB-BD38-9FDFBCB250BB}resource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=bool{3F34FA8A-C19A-40AB-9D17-555D3362344C}resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=bool{4284D3DA-9AC5-419B-8045-949A5E8309AE}resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{428D6779-9889-4B8B-8B9E-AD5629546966}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]{43F0DCEF-21A6-43D8-B966-7B7A896719CE}resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=bool{45D31DD5-FE33-4FC2-A0C8-4CCDD441D2F1}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{45D6C4BC-DC92-44B4-B4D0-618C1DC0314B}resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=bool{479320EC-1DE5-4647-8815-6AC8A065ABBE}resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{501E965D-7E2A-4383-8C8F-5505C5BD214D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{599FBF48-F32D-4A7F-93FD-388CC4220CBA}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{5BF6E3AF-2B65-4DBD-8E08-59B5165C55AA}resource=/Scan Clock;0;ReadMethodType=bool{618E4B9F-A4BA-4CB7-96BC-87EB42C9A8EE}resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6243E5D0-79DF-402F-8A50-B53C5FF5DA98}resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6573F436-992A-4186-B93C-25FA36110953}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{65C9BFE9-D076-4016-8F33-B877F514E4BA}resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=bool{6A4593D3-0BC7-43B4-ADEF-4B9AFE2A709E}resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B3803FA-21DB-4AB6-A8CE-08CACD159105}resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=bool{6DB6A415-F34B-462D-9003-6463AF47A1D6}resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{72420D18-2378-4059-9177-FF96673B6B3A}Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{73B76717-5572-4802-BA31-503777B1510A}resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{764E07AA-36CB-4546-AE7A-B4C9359771A7}resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=bool{78A119D7-C6D2-457D-94E0-A2AD0BDF5E6C}resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7C6BB4F9-44AA-417E-A0EE-872BA2D274CB}resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7DA2B1F7-3BC3-40A5-B33F-A03CD0422223}resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{829CF660-10F4-4789-90C8-2B7EFCFE9A96}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{83335CFE-F24B-40B0-B52B-CC9F69E098DC}resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{84044843-3058-46A9-BD66-9040DB5219F1}resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=bool{8798E7B0-DC62-47F1-9C4C-115A83AD297C}resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=bool{89CC0C6F-61D1-4E0D-9741-7E896879729B}resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=bool{8BDF3502-5B80-484D-8EBF-475C5A4EDD59}resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32{8E2E6BFA-C292-429C-9034-045BF394DD75}resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E6501CE-C594-4F23-BBA3-F3FD1966EBD5}resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E808319-F342-494F-A546-04B7E6AD4F86}resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=bool{90493C34-764B-4DB9-9129-51515ADCB013}resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{909E9819-D2D0-4338-B59B-F4E6AA7039AB}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=bool{92D4FB2E-05BA-42BF-AA64-07D1311098C8}resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=bool{96409551-3F36-4634-8005-4AEB474E3CD9}resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=bool{96C3EFA7-E861-4EBF-83C9-E1AF218AACD3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=bool{99AF8B4F-77C8-42A9-81CB-7D06FEAC4C51}resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=bool{9D16FDC3-761A-47D3-9F16-090952B9DF93}[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{9EDBCFC2-0B59-4D94-8A86-8842917C4B36}cRIO Subresource{9F1D659C-6CEA-44F5-9F02-3D38B7292B84}resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A0733BDF-EDA7-44AC-82CD-EB0ED2694CCB}resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=bool{A2C409CD-9FA7-4207-A56D-7C70D1D6C429}resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=bool{A38E207B-95D1-44DD-858A-AAA53C2A2EE4}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=bool{A49ECA35-5C15-4599-9653-91D633E5E820}resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A4A959E2-EC1C-446A-91F8-E5F0DF041ADE}resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8{A98FB67C-6BBA-4376-A250-0114C7CFF5D5}resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A9D076F8-7203-4469-B2FE-91CE79B5BC88}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=bool{AAF52CFC-2B95-4D50-8BDB-4BAC04201C91}resource=/Chassis Temperature;0;ReadMethodType=i16{AB774714-8D35-40BF-86F8-066E6A8F1028}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{AF2673CD-BFDB-4489-B4AC-7610A070DEE7}resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3198168-FD4B-4BF0-BE27-D33A931E8124}resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=bool{B542CA3B-83BF-43AC-8C1E-39492FC065E0}resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=bool{B7DF432F-881A-46F1-AE34-74783A6DB980}resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BF3371D2-8AC7-4BA1-8135-8CE8C7834BBF}resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C160F4A5-A557-4D2C-94A5-50EA951DD088}resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C331EFF8-513C-415C-B62D-EFEE17754B24}cRIO Subresource{C3ABDF9D-A89E-4561-8B64-A6D08724CF8A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3B8FA71-2860-49DE-938E-1A72CA9B5295}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=bool{C4C096EC-CC95-4A09-813E-AD1BAAAD49FA}resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=bool{C79FB7B5-0CA6-4A63-97B3-1C2DE6AB6E90}resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=bool{C9377557-BDF5-4710-A0B0-D6DC4DFD122B}Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{D3C8C9C9-130D-4791-B2FB-35F6F161F90F}resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D5142CB3-57CC-417D-B6FF-98FBB258185F}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D8333EDB-840D-4440-B9CE-CE31080408D5}resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=bool{DDF6525D-4A0E-4266-BD58-C08100C45FA6}resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{DF8D857A-0EAE-4089-961A-DF6A77009713}resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E234C37E-7F8C-4EBE-ACD6-590F5A61C92F}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool{E234CAD5-52C0-450E-BF93-AF6EB6AC105A}resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8{E51654C2-E6B6-4D34-AD50-410968522D52}resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E7B8FD1D-E793-4F6C-B4D6-B8EF3138DE11}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=bool{E82C1336-89E1-46D4-8224-87CD2735A98A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{E9029A03-9269-478F-B190-93BE4A712ECE}resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=bool{EA023CC2-6328-48A6-8778-6B014504B20D}resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8{ED7CA35A-CBC7-47A9-8C6D-51FC68881467}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8{EE4DA3E6-24E0-474B-805C-ADD2FD81CC80}resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F1F2DD15-226C-415E-9382-888C2ACDC637}resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F8863AE3-7073-45C0-B117-9D7D60315F29}resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=bool{FE524C91-056A-415B-BB9A-58065C304E75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]cRIOAnalogInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]cRIOAnalogOutput[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIODigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]cRIODigitalOutput[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIOHsDigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOHsDigitalOutput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOSsdModul[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]DeviceCalibrationActual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsActual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsFifo"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"Encoder 1 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=boolEncoder 1 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=boolEncoder 2 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=boolFPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1/AI0resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI10resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI11resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI12resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI13resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI14resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI15resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI16resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI17resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI18resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI19resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI1resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI20resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI21resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI22resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI23resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI24resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI25resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI26resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI27resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI28resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI29resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI2resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI30resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI31resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI3resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI4resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI5resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI6resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI7resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI8resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI9resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/DI0resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=boolMod1/DO0resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=boolMod1/Trigresource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=boolMod3/DI1NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=boolMod3/DI2NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=boolMod3/DI4NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=boolMod3/DI5:0NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8Mod3/DI5NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=boolMod4/DO7:0ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DI0resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=boolMod5/DI10resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=boolMod5/DI11resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=boolMod5/DI12resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=boolMod5/DI13resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=boolMod5/DI14resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=boolMod5/DI15:8resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8Mod5/DI15resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=boolMod5/DI16resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=boolMod5/DI17resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=boolMod5/DI18resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=boolMod5/DI19resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=boolMod5/DI1resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=boolMod5/DI20resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=boolMod5/DI21resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=boolMod5/DI22resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=boolMod5/DI23:16resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8Mod5/DI23resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=boolMod5/DI24resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=boolMod5/DI25resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=boolMod5/DI26resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=boolMod5/DI27resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=boolMod5/DI28resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=boolMod5/DI29resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=boolMod5/DI2resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=boolMod5/DI30resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=boolMod5/DI31:0resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32Mod5/DI31:24resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8Mod5/DI31resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=boolMod5/DI3resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=boolMod5/DI4resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=boolMod5/DI5resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=boolMod5/DI6resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=boolMod5/DI7:0resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8Mod5/DI7resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=boolMod5/DI8resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=boolMod5/DI9resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSD Card 0cRIO SubresourceSD Card 1cRIO SubresourceSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSsiSynch1Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0SsiSynch2Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0System Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolTrig 1ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=boolTrig 2ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool</Property>
				<Property Name="Mode" Type="Int">0</Property>
				<Property Name="NI.LV.FPGA.CLIPDeclarationsArraySize" Type="Int">0</Property>
				<Property Name="NI.LV.FPGA.CLIPDeclarationSet" Type="Xml">
<CLIPDeclarationSet>
</CLIPDeclarationSet></Property>
				<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
				<Property Name="Resource Name" Type="Str">RIO0</Property>
				<Property Name="SWEmulationSubMode" Type="UInt">0</Property>
				<Property Name="SWEmulationVIPath" Type="Path"></Property>
				<Property Name="Target Class" Type="Str">cRIO-9114</Property>
				<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
				<Item Name="Devices" Type="Folder">
					<Item Name="Encoder" Type="Folder">
						<Item Name="SsiSynch1" Type="FPGA Memory Block">
							<Property Name="FPGA.PersistentID" Type="Str">{6573F436-992A-4186-B93C-25FA36110953}</Property>
							<Property Name="fullEmulation" Type="Bool">true</Property>
							<Property Name="Memory Latency" Type="UInt">0</Property>
							<Property Name="Multiple Clock Domains" Type="Bool">false</Property>
							<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0</Property>
							<Property Name="NI.LV.FPGA.MEMORY.ActualNumberOfElements" Type="UInt">2</Property>
							<Property Name="NI.LV.FPGA.MEMORY.DataWidth" Type="UInt">0</Property>
							<Property Name="NI.LV.FPGA.MEMORY.DramIncludeByteEnables" Type="Bool">false</Property>
							<Property Name="NI.LV.FPGA.MEMORY.DramMaxOutstandingRequests" Type="Int">32</Property>
							<Property Name="NI.LV.FPGA.MEMORY.DramSelection" Type="Str"></Property>
							<Property Name="NI.LV.FPGA.MEMORY.Init" Type="Bool">true</Property>
							<Property Name="NI.LV.FPGA.MEMORY.InitData" Type="Str">00</Property>
							<Property Name="NI.LV.FPGA.MEMORY.InitVIPath" Type="Str"></Property>
							<Property Name="NI.LV.FPGA.MEMORY.InterfaceAArbitration" Type="UInt">2</Property>
							<Property Name="NI.LV.FPGA.MEMORY.InterfaceBArbitration" Type="UInt">2</Property>
							<Property Name="NI.LV.FPGA.MEMORY.InterfaceConfig" Type="UInt">0</Property>
							<Property Name="NI.LV.FPGA.MEMORY.RequestedNumberOfElements" Type="UInt">1</Property>
							<Property Name="NI.LV.FPGA.MEMORY.Type" Type="UInt">1</Property>
							<Property Name="NI.LV.FPGA.ScriptConfigString" Type="Str">Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0Persist Memory ValuesTRUE;</Property>
							<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
							<Property Name="NI.LV.FPGA.Version" Type="Int">10</Property>
							<Property Name="Type Descriptor" Type="Str">1000800000000001000A402104426F6F6C00000100000000000000</Property>
						</Item>
						<Item Name="SsiSynch2" Type="FPGA Memory Block">
							<Property Name="FPGA.PersistentID" Type="Str">{829CF660-10F4-4789-90C8-2B7EFCFE9A96}</Property>
							<Property Name="fullEmulation" Type="Bool">true</Property>
							<Property Name="Memory Latency" Type="UInt">0</Property>
							<Property Name="Multiple Clock Domains" Type="Bool">false</Property>
							<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0</Property>
							<Property Name="NI.LV.FPGA.MEMORY.ActualNumberOfElements" Type="UInt">2</Property>
							<Property Name="NI.LV.FPGA.MEMORY.DataWidth" Type="UInt">0</Property>
							<Property Name="NI.LV.FPGA.MEMORY.DramIncludeByteEnables" Type="Bool">false</Property>
							<Property Name="NI.LV.FPGA.MEMORY.DramMaxOutstandingRequests" Type="Int">32</Property>
							<Property Name="NI.LV.FPGA.MEMORY.DramSelection" Type="Str"></Property>
							<Property Name="NI.LV.FPGA.MEMORY.Init" Type="Bool">true</Property>
							<Property Name="NI.LV.FPGA.MEMORY.InitData" Type="Str">00</Property>
							<Property Name="NI.LV.FPGA.MEMORY.InitVIPath" Type="Str"></Property>
							<Property Name="NI.LV.FPGA.MEMORY.InterfaceAArbitration" Type="UInt">2</Property>
							<Property Name="NI.LV.FPGA.MEMORY.InterfaceBArbitration" Type="UInt">2</Property>
							<Property Name="NI.LV.FPGA.MEMORY.InterfaceConfig" Type="UInt">0</Property>
							<Property Name="NI.LV.FPGA.MEMORY.RequestedNumberOfElements" Type="UInt">1</Property>
							<Property Name="NI.LV.FPGA.MEMORY.Type" Type="UInt">1</Property>
							<Property Name="NI.LV.FPGA.ScriptConfigString" Type="Str">Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0Persist Memory ValuesTRUE;</Property>
							<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
							<Property Name="NI.LV.FPGA.Version" Type="Int">10</Property>
							<Property Name="Type Descriptor" Type="Str">1000800000000001000A402104426F6F6C00000100000000000000</Property>
						</Item>
						<Item Name="ClockEncoder.vi" Type="VI" URL="../../../labqt/drivers/DAQ/FpgaDevices/Counter/Ssi/SupportFiles/ClockEncoder.vi">
							<Property Name="configString.guid" Type="Str">{0073020B-2D9F-4450-8A6D-FB85A9636099}resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=bool{02D7301B-E6A1-4790-AC7E-B006C2CF5330}resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=bool{03B06DD0-C38E-40B6-B789-2E7364394881}resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=bool{047B03B7-47D4-4337-9691-DDE46857195C}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=bool{059C4215-D03A-439D-8C82-FFCB009DE0A2}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=bool{071122DE-3456-4D34-A2D3-28C6CD377950}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=bool{080F1513-66C2-421C-BFD9-567E9E5D3E1A}resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{0AE0D5EB-8DE3-4DDD-A2F1-7D02906FB4FC}resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8{0B0FD5A8-C8D0-4E34-AB64-10E9EDF189E7}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=bool{113EA66B-7BEB-4F5A-9BDC-A24733C2A8F1}resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=bool{120D309E-6AA7-429C-BAD9-42306571CCA2}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1640CC45-486E-42ED-A03C-88D12527AF0F}resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{19803E27-A2F2-4517-A7B0-73EDF239B4D6}resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1991A0AF-7D17-4493-B5B2-1DCED6DB502E}resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1F2A7A89-9342-457F-BF2A-C26D1A01A2E4}resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=bool{26CF9E8D-7B3D-4828-9ABC-788302291FBE}resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=bool{27335150-337C-458F-AEDF-BDF23C6820CC}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=bool{29555C81-E676-4708-B543-E8C48A518EB3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2AB3B169-20FC-4443-BE33-975E88F509B8}resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=bool{2E7911A3-ACDC-4265-A2CC-02DF0A0A590D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{33A4CA04-47E3-4DF7-BD69-5E12C1DBDD4C}resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=bool{35534149-09DE-4991-A46F-A239DB71862B}resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=bool{366B01FF-7B7A-4B4D-A8EF-A1D853A5965B}resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=bool{370EE507-4F8F-416A-A0BE-1D3395932C93}resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=bool{374BD079-EA0B-414D-8C4A-986E34C79C0E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{37990431-8C69-4CCA-AB20-4037E80322F0}resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=bool{3BD18584-A6E5-469E-8948-BF5DCDBCB8E3}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3BDAD3AA-D7D6-45FD-8782-47F37B3991F8}resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3C9EC768-98CD-43AB-BD38-9FDFBCB250BB}resource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=bool{3F34FA8A-C19A-40AB-9D17-555D3362344C}resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=bool{4284D3DA-9AC5-419B-8045-949A5E8309AE}resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{428D6779-9889-4B8B-8B9E-AD5629546966}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]{43F0DCEF-21A6-43D8-B966-7B7A896719CE}resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=bool{45D31DD5-FE33-4FC2-A0C8-4CCDD441D2F1}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{45D6C4BC-DC92-44B4-B4D0-618C1DC0314B}resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=bool{479320EC-1DE5-4647-8815-6AC8A065ABBE}resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{501E965D-7E2A-4383-8C8F-5505C5BD214D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{599FBF48-F32D-4A7F-93FD-388CC4220CBA}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{5BF6E3AF-2B65-4DBD-8E08-59B5165C55AA}resource=/Scan Clock;0;ReadMethodType=bool{618E4B9F-A4BA-4CB7-96BC-87EB42C9A8EE}resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6243E5D0-79DF-402F-8A50-B53C5FF5DA98}resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6573F436-992A-4186-B93C-25FA36110953}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{65C9BFE9-D076-4016-8F33-B877F514E4BA}resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=bool{6A4593D3-0BC7-43B4-ADEF-4B9AFE2A709E}resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B3803FA-21DB-4AB6-A8CE-08CACD159105}resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=bool{6DB6A415-F34B-462D-9003-6463AF47A1D6}resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{72420D18-2378-4059-9177-FF96673B6B3A}Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{73B76717-5572-4802-BA31-503777B1510A}resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{764E07AA-36CB-4546-AE7A-B4C9359771A7}resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=bool{78A119D7-C6D2-457D-94E0-A2AD0BDF5E6C}resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7C6BB4F9-44AA-417E-A0EE-872BA2D274CB}resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7DA2B1F7-3BC3-40A5-B33F-A03CD0422223}resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{829CF660-10F4-4789-90C8-2B7EFCFE9A96}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{83335CFE-F24B-40B0-B52B-CC9F69E098DC}resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{84044843-3058-46A9-BD66-9040DB5219F1}resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=bool{8798E7B0-DC62-47F1-9C4C-115A83AD297C}resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=bool{89CC0C6F-61D1-4E0D-9741-7E896879729B}resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=bool{8BDF3502-5B80-484D-8EBF-475C5A4EDD59}resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32{8E2E6BFA-C292-429C-9034-045BF394DD75}resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E6501CE-C594-4F23-BBA3-F3FD1966EBD5}resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E808319-F342-494F-A546-04B7E6AD4F86}resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=bool{90493C34-764B-4DB9-9129-51515ADCB013}resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{909E9819-D2D0-4338-B59B-F4E6AA7039AB}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=bool{92D4FB2E-05BA-42BF-AA64-07D1311098C8}resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=bool{96409551-3F36-4634-8005-4AEB474E3CD9}resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=bool{96C3EFA7-E861-4EBF-83C9-E1AF218AACD3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=bool{99AF8B4F-77C8-42A9-81CB-7D06FEAC4C51}resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=bool{9D16FDC3-761A-47D3-9F16-090952B9DF93}[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{9EDBCFC2-0B59-4D94-8A86-8842917C4B36}cRIO Subresource{9F1D659C-6CEA-44F5-9F02-3D38B7292B84}resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A0733BDF-EDA7-44AC-82CD-EB0ED2694CCB}resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=bool{A2C409CD-9FA7-4207-A56D-7C70D1D6C429}resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=bool{A38E207B-95D1-44DD-858A-AAA53C2A2EE4}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=bool{A49ECA35-5C15-4599-9653-91D633E5E820}resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A4A959E2-EC1C-446A-91F8-E5F0DF041ADE}resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8{A98FB67C-6BBA-4376-A250-0114C7CFF5D5}resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A9D076F8-7203-4469-B2FE-91CE79B5BC88}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=bool{AAF52CFC-2B95-4D50-8BDB-4BAC04201C91}resource=/Chassis Temperature;0;ReadMethodType=i16{AB774714-8D35-40BF-86F8-066E6A8F1028}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{AF2673CD-BFDB-4489-B4AC-7610A070DEE7}resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3198168-FD4B-4BF0-BE27-D33A931E8124}resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=bool{B542CA3B-83BF-43AC-8C1E-39492FC065E0}resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=bool{B7DF432F-881A-46F1-AE34-74783A6DB980}resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BF3371D2-8AC7-4BA1-8135-8CE8C7834BBF}resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C160F4A5-A557-4D2C-94A5-50EA951DD088}resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C331EFF8-513C-415C-B62D-EFEE17754B24}cRIO Subresource{C3ABDF9D-A89E-4561-8B64-A6D08724CF8A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3B8FA71-2860-49DE-938E-1A72CA9B5295}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=bool{C4C096EC-CC95-4A09-813E-AD1BAAAD49FA}resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=bool{C79FB7B5-0CA6-4A63-97B3-1C2DE6AB6E90}resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=bool{C9377557-BDF5-4710-A0B0-D6DC4DFD122B}Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{D3C8C9C9-130D-4791-B2FB-35F6F161F90F}resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D5142CB3-57CC-417D-B6FF-98FBB258185F}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D8333EDB-840D-4440-B9CE-CE31080408D5}resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=bool{DDF6525D-4A0E-4266-BD58-C08100C45FA6}resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{DF8D857A-0EAE-4089-961A-DF6A77009713}resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E234C37E-7F8C-4EBE-ACD6-590F5A61C92F}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool{E234CAD5-52C0-450E-BF93-AF6EB6AC105A}resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8{E51654C2-E6B6-4D34-AD50-410968522D52}resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E7B8FD1D-E793-4F6C-B4D6-B8EF3138DE11}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=bool{E82C1336-89E1-46D4-8224-87CD2735A98A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{E9029A03-9269-478F-B190-93BE4A712ECE}resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=bool{EA023CC2-6328-48A6-8778-6B014504B20D}resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8{ED7CA35A-CBC7-47A9-8C6D-51FC68881467}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8{EE4DA3E6-24E0-474B-805C-ADD2FD81CC80}resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F1F2DD15-226C-415E-9382-888C2ACDC637}resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F8863AE3-7073-45C0-B117-9D7D60315F29}resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=bool{FE524C91-056A-415B-BB9A-58065C304E75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
							<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]cRIOAnalogInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]cRIOAnalogOutput[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIODigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]cRIODigitalOutput[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIOHsDigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOHsDigitalOutput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOSsdModul[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]DeviceCalibrationActual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsActual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsFifo"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"Encoder 1 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=boolEncoder 1 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=boolEncoder 2 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=boolFPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1/AI0resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI10resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI11resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI12resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI13resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI14resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI15resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI16resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI17resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI18resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI19resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI1resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI20resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI21resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI22resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI23resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI24resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI25resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI26resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI27resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI28resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI29resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI2resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI30resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI31resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI3resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI4resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI5resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI6resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI7resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI8resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI9resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/DI0resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=boolMod1/DO0resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=boolMod1/Trigresource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=boolMod3/DI1NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=boolMod3/DI2NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=boolMod3/DI4NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=boolMod3/DI5:0NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8Mod3/DI5NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=boolMod4/DO7:0ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DI0resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=boolMod5/DI10resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=boolMod5/DI11resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=boolMod5/DI12resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=boolMod5/DI13resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=boolMod5/DI14resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=boolMod5/DI15:8resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8Mod5/DI15resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=boolMod5/DI16resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=boolMod5/DI17resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=boolMod5/DI18resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=boolMod5/DI19resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=boolMod5/DI1resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=boolMod5/DI20resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=boolMod5/DI21resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=boolMod5/DI22resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=boolMod5/DI23:16resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8Mod5/DI23resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=boolMod5/DI24resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=boolMod5/DI25resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=boolMod5/DI26resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=boolMod5/DI27resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=boolMod5/DI28resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=boolMod5/DI29resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=boolMod5/DI2resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=boolMod5/DI30resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=boolMod5/DI31:0resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32Mod5/DI31:24resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8Mod5/DI31resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=boolMod5/DI3resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=boolMod5/DI4resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=boolMod5/DI5resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=boolMod5/DI6resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=boolMod5/DI7:0resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8Mod5/DI7resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=boolMod5/DI8resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=boolMod5/DI9resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSD Card 0cRIO SubresourceSD Card 1cRIO SubresourceSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSsiSynch1Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0SsiSynch2Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0System Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolTrig 1ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=boolTrig 2ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool</Property>
						</Item>
						<Item Name="SsiClocker.vi" Type="VI" URL="../../../labqt/drivers/DAQ/FpgaDevices/Counter/Ssi/SupportFiles/SsiClocker.vi">
							<Property Name="BuildSpec" Type="Str">{BDA7C1D8-F864-497F-8770-2212E2AAA489}</Property>
							<Property Name="configString.guid" Type="Str">{0073020B-2D9F-4450-8A6D-FB85A9636099}resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=bool{02D7301B-E6A1-4790-AC7E-B006C2CF5330}resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=bool{03B06DD0-C38E-40B6-B789-2E7364394881}resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=bool{047B03B7-47D4-4337-9691-DDE46857195C}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=bool{059C4215-D03A-439D-8C82-FFCB009DE0A2}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=bool{071122DE-3456-4D34-A2D3-28C6CD377950}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=bool{080F1513-66C2-421C-BFD9-567E9E5D3E1A}resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{0AE0D5EB-8DE3-4DDD-A2F1-7D02906FB4FC}resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8{0B0FD5A8-C8D0-4E34-AB64-10E9EDF189E7}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=bool{113EA66B-7BEB-4F5A-9BDC-A24733C2A8F1}resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=bool{120D309E-6AA7-429C-BAD9-42306571CCA2}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1640CC45-486E-42ED-A03C-88D12527AF0F}resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{19803E27-A2F2-4517-A7B0-73EDF239B4D6}resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1991A0AF-7D17-4493-B5B2-1DCED6DB502E}resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1F2A7A89-9342-457F-BF2A-C26D1A01A2E4}resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=bool{26CF9E8D-7B3D-4828-9ABC-788302291FBE}resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=bool{27335150-337C-458F-AEDF-BDF23C6820CC}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=bool{29555C81-E676-4708-B543-E8C48A518EB3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2AB3B169-20FC-4443-BE33-975E88F509B8}resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=bool{2E7911A3-ACDC-4265-A2CC-02DF0A0A590D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{33A4CA04-47E3-4DF7-BD69-5E12C1DBDD4C}resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=bool{35534149-09DE-4991-A46F-A239DB71862B}resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=bool{366B01FF-7B7A-4B4D-A8EF-A1D853A5965B}resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=bool{370EE507-4F8F-416A-A0BE-1D3395932C93}resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=bool{374BD079-EA0B-414D-8C4A-986E34C79C0E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{37990431-8C69-4CCA-AB20-4037E80322F0}resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=bool{3BD18584-A6E5-469E-8948-BF5DCDBCB8E3}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3BDAD3AA-D7D6-45FD-8782-47F37B3991F8}resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3C9EC768-98CD-43AB-BD38-9FDFBCB250BB}resource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=bool{3F34FA8A-C19A-40AB-9D17-555D3362344C}resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=bool{4284D3DA-9AC5-419B-8045-949A5E8309AE}resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{428D6779-9889-4B8B-8B9E-AD5629546966}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]{43F0DCEF-21A6-43D8-B966-7B7A896719CE}resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=bool{45D31DD5-FE33-4FC2-A0C8-4CCDD441D2F1}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{45D6C4BC-DC92-44B4-B4D0-618C1DC0314B}resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=bool{479320EC-1DE5-4647-8815-6AC8A065ABBE}resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{501E965D-7E2A-4383-8C8F-5505C5BD214D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{599FBF48-F32D-4A7F-93FD-388CC4220CBA}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{5BF6E3AF-2B65-4DBD-8E08-59B5165C55AA}resource=/Scan Clock;0;ReadMethodType=bool{618E4B9F-A4BA-4CB7-96BC-87EB42C9A8EE}resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6243E5D0-79DF-402F-8A50-B53C5FF5DA98}resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6573F436-992A-4186-B93C-25FA36110953}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{65C9BFE9-D076-4016-8F33-B877F514E4BA}resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=bool{6A4593D3-0BC7-43B4-ADEF-4B9AFE2A709E}resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B3803FA-21DB-4AB6-A8CE-08CACD159105}resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=bool{6DB6A415-F34B-462D-9003-6463AF47A1D6}resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{72420D18-2378-4059-9177-FF96673B6B3A}Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{73B76717-5572-4802-BA31-503777B1510A}resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{764E07AA-36CB-4546-AE7A-B4C9359771A7}resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=bool{78A119D7-C6D2-457D-94E0-A2AD0BDF5E6C}resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7C6BB4F9-44AA-417E-A0EE-872BA2D274CB}resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7DA2B1F7-3BC3-40A5-B33F-A03CD0422223}resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{829CF660-10F4-4789-90C8-2B7EFCFE9A96}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{83335CFE-F24B-40B0-B52B-CC9F69E098DC}resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{84044843-3058-46A9-BD66-9040DB5219F1}resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=bool{8798E7B0-DC62-47F1-9C4C-115A83AD297C}resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=bool{89CC0C6F-61D1-4E0D-9741-7E896879729B}resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=bool{8BDF3502-5B80-484D-8EBF-475C5A4EDD59}resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32{8E2E6BFA-C292-429C-9034-045BF394DD75}resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E6501CE-C594-4F23-BBA3-F3FD1966EBD5}resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E808319-F342-494F-A546-04B7E6AD4F86}resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=bool{90493C34-764B-4DB9-9129-51515ADCB013}resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{909E9819-D2D0-4338-B59B-F4E6AA7039AB}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=bool{92D4FB2E-05BA-42BF-AA64-07D1311098C8}resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=bool{96409551-3F36-4634-8005-4AEB474E3CD9}resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=bool{96C3EFA7-E861-4EBF-83C9-E1AF218AACD3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=bool{99AF8B4F-77C8-42A9-81CB-7D06FEAC4C51}resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=bool{9D16FDC3-761A-47D3-9F16-090952B9DF93}[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{9EDBCFC2-0B59-4D94-8A86-8842917C4B36}cRIO Subresource{9F1D659C-6CEA-44F5-9F02-3D38B7292B84}resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A0733BDF-EDA7-44AC-82CD-EB0ED2694CCB}resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=bool{A2C409CD-9FA7-4207-A56D-7C70D1D6C429}resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=bool{A38E207B-95D1-44DD-858A-AAA53C2A2EE4}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=bool{A49ECA35-5C15-4599-9653-91D633E5E820}resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A4A959E2-EC1C-446A-91F8-E5F0DF041ADE}resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8{A98FB67C-6BBA-4376-A250-0114C7CFF5D5}resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A9D076F8-7203-4469-B2FE-91CE79B5BC88}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=bool{AAF52CFC-2B95-4D50-8BDB-4BAC04201C91}resource=/Chassis Temperature;0;ReadMethodType=i16{AB774714-8D35-40BF-86F8-066E6A8F1028}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{AF2673CD-BFDB-4489-B4AC-7610A070DEE7}resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3198168-FD4B-4BF0-BE27-D33A931E8124}resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=bool{B542CA3B-83BF-43AC-8C1E-39492FC065E0}resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=bool{B7DF432F-881A-46F1-AE34-74783A6DB980}resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BF3371D2-8AC7-4BA1-8135-8CE8C7834BBF}resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C160F4A5-A557-4D2C-94A5-50EA951DD088}resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C331EFF8-513C-415C-B62D-EFEE17754B24}cRIO Subresource{C3ABDF9D-A89E-4561-8B64-A6D08724CF8A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3B8FA71-2860-49DE-938E-1A72CA9B5295}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=bool{C4C096EC-CC95-4A09-813E-AD1BAAAD49FA}resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=bool{C79FB7B5-0CA6-4A63-97B3-1C2DE6AB6E90}resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=bool{C9377557-BDF5-4710-A0B0-D6DC4DFD122B}Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{D3C8C9C9-130D-4791-B2FB-35F6F161F90F}resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D5142CB3-57CC-417D-B6FF-98FBB258185F}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D8333EDB-840D-4440-B9CE-CE31080408D5}resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=bool{DDF6525D-4A0E-4266-BD58-C08100C45FA6}resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{DF8D857A-0EAE-4089-961A-DF6A77009713}resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E234C37E-7F8C-4EBE-ACD6-590F5A61C92F}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool{E234CAD5-52C0-450E-BF93-AF6EB6AC105A}resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8{E51654C2-E6B6-4D34-AD50-410968522D52}resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E7B8FD1D-E793-4F6C-B4D6-B8EF3138DE11}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=bool{E82C1336-89E1-46D4-8224-87CD2735A98A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{E9029A03-9269-478F-B190-93BE4A712ECE}resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=bool{EA023CC2-6328-48A6-8778-6B014504B20D}resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8{ED7CA35A-CBC7-47A9-8C6D-51FC68881467}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8{EE4DA3E6-24E0-474B-805C-ADD2FD81CC80}resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F1F2DD15-226C-415E-9382-888C2ACDC637}resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F8863AE3-7073-45C0-B117-9D7D60315F29}resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=bool{FE524C91-056A-415B-BB9A-58065C304E75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
							<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]cRIOAnalogInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]cRIOAnalogOutput[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIODigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]cRIODigitalOutput[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIOHsDigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOHsDigitalOutput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOSsdModul[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]DeviceCalibrationActual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsActual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsFifo"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"Encoder 1 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=boolEncoder 1 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=boolEncoder 2 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=boolFPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1/AI0resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI10resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI11resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI12resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI13resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI14resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI15resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI16resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI17resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI18resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI19resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI1resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI20resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI21resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI22resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI23resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI24resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI25resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI26resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI27resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI28resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI29resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI2resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI30resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI31resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI3resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI4resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI5resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI6resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI7resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI8resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI9resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/DI0resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=boolMod1/DO0resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=boolMod1/Trigresource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=boolMod3/DI1NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=boolMod3/DI2NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=boolMod3/DI4NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=boolMod3/DI5:0NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8Mod3/DI5NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=boolMod4/DO7:0ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DI0resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=boolMod5/DI10resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=boolMod5/DI11resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=boolMod5/DI12resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=boolMod5/DI13resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=boolMod5/DI14resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=boolMod5/DI15:8resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8Mod5/DI15resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=boolMod5/DI16resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=boolMod5/DI17resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=boolMod5/DI18resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=boolMod5/DI19resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=boolMod5/DI1resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=boolMod5/DI20resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=boolMod5/DI21resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=boolMod5/DI22resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=boolMod5/DI23:16resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8Mod5/DI23resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=boolMod5/DI24resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=boolMod5/DI25resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=boolMod5/DI26resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=boolMod5/DI27resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=boolMod5/DI28resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=boolMod5/DI29resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=boolMod5/DI2resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=boolMod5/DI30resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=boolMod5/DI31:0resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32Mod5/DI31:24resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8Mod5/DI31resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=boolMod5/DI3resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=boolMod5/DI4resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=boolMod5/DI5resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=boolMod5/DI6resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=boolMod5/DI7:0resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8Mod5/DI7resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=boolMod5/DI8resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=boolMod5/DI9resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSD Card 0cRIO SubresourceSD Card 1cRIO SubresourceSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSsiSynch1Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0SsiSynch2Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0System Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolTrig 1ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=boolTrig 2ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool</Property>
						</Item>
					</Item>
					<Item Name="SsiMasterSlaveEncoder.vi" Type="VI" URL="../../../labqt/drivers/DAQ/FpgaDevices/Counter/Ssi/SsiMasterSlaveEncoder.vi">
						<Property Name="configString.guid" Type="Str">{0073020B-2D9F-4450-8A6D-FB85A9636099}resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=bool{02D7301B-E6A1-4790-AC7E-B006C2CF5330}resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=bool{03B06DD0-C38E-40B6-B789-2E7364394881}resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=bool{047B03B7-47D4-4337-9691-DDE46857195C}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=bool{059C4215-D03A-439D-8C82-FFCB009DE0A2}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=bool{071122DE-3456-4D34-A2D3-28C6CD377950}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=bool{080F1513-66C2-421C-BFD9-567E9E5D3E1A}resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{0AE0D5EB-8DE3-4DDD-A2F1-7D02906FB4FC}resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8{0B0FD5A8-C8D0-4E34-AB64-10E9EDF189E7}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=bool{113EA66B-7BEB-4F5A-9BDC-A24733C2A8F1}resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=bool{120D309E-6AA7-429C-BAD9-42306571CCA2}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1640CC45-486E-42ED-A03C-88D12527AF0F}resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{19803E27-A2F2-4517-A7B0-73EDF239B4D6}resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1991A0AF-7D17-4493-B5B2-1DCED6DB502E}resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1F2A7A89-9342-457F-BF2A-C26D1A01A2E4}resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=bool{26CF9E8D-7B3D-4828-9ABC-788302291FBE}resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=bool{27335150-337C-458F-AEDF-BDF23C6820CC}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=bool{29555C81-E676-4708-B543-E8C48A518EB3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2AB3B169-20FC-4443-BE33-975E88F509B8}resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=bool{2E7911A3-ACDC-4265-A2CC-02DF0A0A590D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{33A4CA04-47E3-4DF7-BD69-5E12C1DBDD4C}resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=bool{35534149-09DE-4991-A46F-A239DB71862B}resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=bool{366B01FF-7B7A-4B4D-A8EF-A1D853A5965B}resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=bool{370EE507-4F8F-416A-A0BE-1D3395932C93}resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=bool{374BD079-EA0B-414D-8C4A-986E34C79C0E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{37990431-8C69-4CCA-AB20-4037E80322F0}resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=bool{3BD18584-A6E5-469E-8948-BF5DCDBCB8E3}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3BDAD3AA-D7D6-45FD-8782-47F37B3991F8}resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3C9EC768-98CD-43AB-BD38-9FDFBCB250BB}resource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=bool{3F34FA8A-C19A-40AB-9D17-555D3362344C}resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=bool{4284D3DA-9AC5-419B-8045-949A5E8309AE}resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{428D6779-9889-4B8B-8B9E-AD5629546966}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]{43F0DCEF-21A6-43D8-B966-7B7A896719CE}resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=bool{45D31DD5-FE33-4FC2-A0C8-4CCDD441D2F1}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{45D6C4BC-DC92-44B4-B4D0-618C1DC0314B}resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=bool{479320EC-1DE5-4647-8815-6AC8A065ABBE}resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{501E965D-7E2A-4383-8C8F-5505C5BD214D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{599FBF48-F32D-4A7F-93FD-388CC4220CBA}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{5BF6E3AF-2B65-4DBD-8E08-59B5165C55AA}resource=/Scan Clock;0;ReadMethodType=bool{618E4B9F-A4BA-4CB7-96BC-87EB42C9A8EE}resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6243E5D0-79DF-402F-8A50-B53C5FF5DA98}resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6573F436-992A-4186-B93C-25FA36110953}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{65C9BFE9-D076-4016-8F33-B877F514E4BA}resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=bool{6A4593D3-0BC7-43B4-ADEF-4B9AFE2A709E}resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B3803FA-21DB-4AB6-A8CE-08CACD159105}resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=bool{6DB6A415-F34B-462D-9003-6463AF47A1D6}resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{72420D18-2378-4059-9177-FF96673B6B3A}Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{73B76717-5572-4802-BA31-503777B1510A}resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{764E07AA-36CB-4546-AE7A-B4C9359771A7}resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=bool{78A119D7-C6D2-457D-94E0-A2AD0BDF5E6C}resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7C6BB4F9-44AA-417E-A0EE-872BA2D274CB}resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7DA2B1F7-3BC3-40A5-B33F-A03CD0422223}resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{829CF660-10F4-4789-90C8-2B7EFCFE9A96}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{83335CFE-F24B-40B0-B52B-CC9F69E098DC}resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{84044843-3058-46A9-BD66-9040DB5219F1}resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=bool{8798E7B0-DC62-47F1-9C4C-115A83AD297C}resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=bool{89CC0C6F-61D1-4E0D-9741-7E896879729B}resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=bool{8BDF3502-5B80-484D-8EBF-475C5A4EDD59}resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32{8E2E6BFA-C292-429C-9034-045BF394DD75}resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E6501CE-C594-4F23-BBA3-F3FD1966EBD5}resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E808319-F342-494F-A546-04B7E6AD4F86}resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=bool{90493C34-764B-4DB9-9129-51515ADCB013}resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{909E9819-D2D0-4338-B59B-F4E6AA7039AB}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=bool{92D4FB2E-05BA-42BF-AA64-07D1311098C8}resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=bool{96409551-3F36-4634-8005-4AEB474E3CD9}resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=bool{96C3EFA7-E861-4EBF-83C9-E1AF218AACD3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=bool{99AF8B4F-77C8-42A9-81CB-7D06FEAC4C51}resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=bool{9D16FDC3-761A-47D3-9F16-090952B9DF93}[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{9EDBCFC2-0B59-4D94-8A86-8842917C4B36}cRIO Subresource{9F1D659C-6CEA-44F5-9F02-3D38B7292B84}resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A0733BDF-EDA7-44AC-82CD-EB0ED2694CCB}resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=bool{A2C409CD-9FA7-4207-A56D-7C70D1D6C429}resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=bool{A38E207B-95D1-44DD-858A-AAA53C2A2EE4}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=bool{A49ECA35-5C15-4599-9653-91D633E5E820}resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A4A959E2-EC1C-446A-91F8-E5F0DF041ADE}resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8{A98FB67C-6BBA-4376-A250-0114C7CFF5D5}resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A9D076F8-7203-4469-B2FE-91CE79B5BC88}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=bool{AAF52CFC-2B95-4D50-8BDB-4BAC04201C91}resource=/Chassis Temperature;0;ReadMethodType=i16{AB774714-8D35-40BF-86F8-066E6A8F1028}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{AF2673CD-BFDB-4489-B4AC-7610A070DEE7}resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3198168-FD4B-4BF0-BE27-D33A931E8124}resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=bool{B542CA3B-83BF-43AC-8C1E-39492FC065E0}resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=bool{B7DF432F-881A-46F1-AE34-74783A6DB980}resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BF3371D2-8AC7-4BA1-8135-8CE8C7834BBF}resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C160F4A5-A557-4D2C-94A5-50EA951DD088}resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C331EFF8-513C-415C-B62D-EFEE17754B24}cRIO Subresource{C3ABDF9D-A89E-4561-8B64-A6D08724CF8A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3B8FA71-2860-49DE-938E-1A72CA9B5295}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=bool{C4C096EC-CC95-4A09-813E-AD1BAAAD49FA}resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=bool{C79FB7B5-0CA6-4A63-97B3-1C2DE6AB6E90}resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=bool{C9377557-BDF5-4710-A0B0-D6DC4DFD122B}Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{D3C8C9C9-130D-4791-B2FB-35F6F161F90F}resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D5142CB3-57CC-417D-B6FF-98FBB258185F}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D8333EDB-840D-4440-B9CE-CE31080408D5}resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=bool{DDF6525D-4A0E-4266-BD58-C08100C45FA6}resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{DF8D857A-0EAE-4089-961A-DF6A77009713}resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E234C37E-7F8C-4EBE-ACD6-590F5A61C92F}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool{E234CAD5-52C0-450E-BF93-AF6EB6AC105A}resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8{E51654C2-E6B6-4D34-AD50-410968522D52}resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E7B8FD1D-E793-4F6C-B4D6-B8EF3138DE11}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=bool{E82C1336-89E1-46D4-8224-87CD2735A98A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{E9029A03-9269-478F-B190-93BE4A712ECE}resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=bool{EA023CC2-6328-48A6-8778-6B014504B20D}resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8{ED7CA35A-CBC7-47A9-8C6D-51FC68881467}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8{EE4DA3E6-24E0-474B-805C-ADD2FD81CC80}resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F1F2DD15-226C-415E-9382-888C2ACDC637}resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F8863AE3-7073-45C0-B117-9D7D60315F29}resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=bool{FE524C91-056A-415B-BB9A-58065C304E75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
						<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]cRIOAnalogInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]cRIOAnalogOutput[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIODigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]cRIODigitalOutput[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIOHsDigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOHsDigitalOutput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOSsdModul[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]DeviceCalibrationActual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsActual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsFifo"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"Encoder 1 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=boolEncoder 1 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=boolEncoder 2 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=boolFPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1/AI0resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI10resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI11resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI12resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI13resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI14resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI15resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI16resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI17resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI18resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI19resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI1resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI20resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI21resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI22resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI23resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI24resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI25resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI26resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI27resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI28resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI29resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI2resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI30resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI31resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI3resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI4resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI5resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI6resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI7resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI8resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI9resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/DI0resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=boolMod1/DO0resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=boolMod1/Trigresource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=boolMod3/DI1NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=boolMod3/DI2NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=boolMod3/DI4NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=boolMod3/DI5:0NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8Mod3/DI5NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=boolMod4/DO7:0ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DI0resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=boolMod5/DI10resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=boolMod5/DI11resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=boolMod5/DI12resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=boolMod5/DI13resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=boolMod5/DI14resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=boolMod5/DI15:8resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8Mod5/DI15resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=boolMod5/DI16resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=boolMod5/DI17resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=boolMod5/DI18resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=boolMod5/DI19resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=boolMod5/DI1resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=boolMod5/DI20resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=boolMod5/DI21resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=boolMod5/DI22resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=boolMod5/DI23:16resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8Mod5/DI23resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=boolMod5/DI24resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=boolMod5/DI25resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=boolMod5/DI26resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=boolMod5/DI27resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=boolMod5/DI28resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=boolMod5/DI29resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=boolMod5/DI2resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=boolMod5/DI30resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=boolMod5/DI31:0resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32Mod5/DI31:24resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8Mod5/DI31resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=boolMod5/DI3resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=boolMod5/DI4resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=boolMod5/DI5resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=boolMod5/DI6resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=boolMod5/DI7:0resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8Mod5/DI7resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=boolMod5/DI8resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=boolMod5/DI9resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSD Card 0cRIO SubresourceSD Card 1cRIO SubresourceSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSsiSynch1Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0SsiSynch2Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0System Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolTrig 1ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=boolTrig 2ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool</Property>
						<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\sources\oceanharvesting\target\FPGA Bitfiles\target_FPGATarget2_DualSsiEncoders_E38274E0.lvbitx</Property>
					</Item>
				</Item>
				<Item Name="DAQ" Type="Folder">
					<Item Name="DeviceMeasurements" Type="FPGA Memory Block">
						<Property Name="FPGA.PersistentID" Type="Str">{72420D18-2378-4059-9177-FF96673B6B3A}</Property>
						<Property Name="fullEmulation" Type="Bool">true</Property>
						<Property Name="Memory Latency" Type="UInt">1</Property>
						<Property Name="Multiple Clock Domains" Type="Bool">false</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1</Property>
						<Property Name="NI.LV.FPGA.MEMORY.ActualNumberOfElements" Type="UInt">66</Property>
						<Property Name="NI.LV.FPGA.MEMORY.DataWidth" Type="UInt">9</Property>
						<Property Name="NI.LV.FPGA.MEMORY.DramIncludeByteEnables" Type="Bool">false</Property>
						<Property Name="NI.LV.FPGA.MEMORY.DramMaxOutstandingRequests" Type="Int">32</Property>
						<Property Name="NI.LV.FPGA.MEMORY.DramSelection" Type="Str"></Property>
						<Property Name="NI.LV.FPGA.MEMORY.Init" Type="Bool">true</Property>
						<Property Name="NI.LV.FPGA.MEMORY.InitData" Type="Str">000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000</Property>
						<Property Name="NI.LV.FPGA.MEMORY.InitVIPath" Type="Str"></Property>
						<Property Name="NI.LV.FPGA.MEMORY.InterfaceAArbitration" Type="UInt">0</Property>
						<Property Name="NI.LV.FPGA.MEMORY.InterfaceBArbitration" Type="UInt">0</Property>
						<Property Name="NI.LV.FPGA.MEMORY.InterfaceConfig" Type="UInt">0</Property>
						<Property Name="NI.LV.FPGA.MEMORY.RequestedNumberOfElements" Type="UInt">66</Property>
						<Property Name="NI.LV.FPGA.MEMORY.Type" Type="UInt">2</Property>
						<Property Name="NI.LV.FPGA.ScriptConfigString" Type="Str">Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1Persist Memory ValuesTRUE;</Property>
						<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
						<Property Name="NI.LV.FPGA.Version" Type="Int">10</Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000</Property>
					</Item>
					<Item Name="DeviceCalibration" Type="FPGA Memory Block">
						<Property Name="FPGA.PersistentID" Type="Str">{C9377557-BDF5-4710-A0B0-D6DC4DFD122B}</Property>
						<Property Name="fullEmulation" Type="Bool">true</Property>
						<Property Name="Memory Latency" Type="UInt">1</Property>
						<Property Name="Multiple Clock Domains" Type="Bool">false</Property>
						<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1</Property>
						<Property Name="NI.LV.FPGA.MEMORY.ActualNumberOfElements" Type="UInt">132</Property>
						<Property Name="NI.LV.FPGA.MEMORY.DataWidth" Type="UInt">9</Property>
						<Property Name="NI.LV.FPGA.MEMORY.DramIncludeByteEnables" Type="Bool">false</Property>
						<Property Name="NI.LV.FPGA.MEMORY.DramMaxOutstandingRequests" Type="Int">32</Property>
						<Property Name="NI.LV.FPGA.MEMORY.DramSelection" Type="Str"></Property>
						<Property Name="NI.LV.FPGA.MEMORY.Init" Type="Bool">true</Property>
						<Property Name="NI.LV.FPGA.MEMORY.InitData" Type="Str">000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000001000000000000000000000000000000010000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000</Property>
						<Property Name="NI.LV.FPGA.MEMORY.InitVIPath" Type="Str"></Property>
						<Property Name="NI.LV.FPGA.MEMORY.InterfaceAArbitration" Type="UInt">0</Property>
						<Property Name="NI.LV.FPGA.MEMORY.InterfaceBArbitration" Type="UInt">0</Property>
						<Property Name="NI.LV.FPGA.MEMORY.InterfaceConfig" Type="UInt">0</Property>
						<Property Name="NI.LV.FPGA.MEMORY.RequestedNumberOfElements" Type="UInt">132</Property>
						<Property Name="NI.LV.FPGA.MEMORY.Type" Type="UInt">2</Property>
						<Property Name="NI.LV.FPGA.ScriptConfigString" Type="Str">Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1Persist Memory ValuesTRUE;</Property>
						<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
						<Property Name="NI.LV.FPGA.Version" Type="Int">10</Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000</Property>
					</Item>
					<Item Name="DeviceMeasurementsFifo" Type="FPGA FIFO">
						<Property Name="Actual Number of Elements" Type="UInt">1023</Property>
						<Property Name="Arbitration for Read" Type="UInt">1</Property>
						<Property Name="Arbitration for Write" Type="UInt">0</Property>
						<Property Name="Control Logic" Type="UInt">0</Property>
						<Property Name="Data Type" Type="UInt">9</Property>
						<Property Name="Disable on Overflow/Underflow" Type="Bool">false</Property>
						<Property Name="fifo.configuration" Type="Str">"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"</Property>
						<Property Name="fifo.configured" Type="Bool">true</Property>
						<Property Name="fifo.projectItemValid" Type="Bool">true</Property>
						<Property Name="fifo.valid" Type="Bool">true</Property>
						<Property Name="fifo.version" Type="Int">12</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{599FBF48-F32D-4A7F-93FD-388CC4220CBA}</Property>
						<Property Name="Local" Type="Bool">false</Property>
						<Property Name="Memory Type" Type="UInt">2</Property>
						<Property Name="Number Of Elements Per Read" Type="UInt">1</Property>
						<Property Name="Number Of Elements Per Write" Type="UInt">1</Property>
						<Property Name="Requested Number of Elements" Type="UInt">1023</Property>
						<Property Name="Type" Type="UInt">2</Property>
						<Property Name="Type Descriptor" Type="Str">1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000</Property>
					</Item>
					<Item Name="FifoBufferedOutput.vi" Type="VI" URL="../../../labqt/drivers/DAQ/CRioBufferedDeviceReader/FPGA/FifoBufferedOutput.vi">
						<Property Name="configString.guid" Type="Str">{0073020B-2D9F-4450-8A6D-FB85A9636099}resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=bool{02D7301B-E6A1-4790-AC7E-B006C2CF5330}resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=bool{03B06DD0-C38E-40B6-B789-2E7364394881}resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=bool{047B03B7-47D4-4337-9691-DDE46857195C}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=bool{059C4215-D03A-439D-8C82-FFCB009DE0A2}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=bool{071122DE-3456-4D34-A2D3-28C6CD377950}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=bool{080F1513-66C2-421C-BFD9-567E9E5D3E1A}resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{0AE0D5EB-8DE3-4DDD-A2F1-7D02906FB4FC}resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8{0B0FD5A8-C8D0-4E34-AB64-10E9EDF189E7}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=bool{113EA66B-7BEB-4F5A-9BDC-A24733C2A8F1}resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=bool{120D309E-6AA7-429C-BAD9-42306571CCA2}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1640CC45-486E-42ED-A03C-88D12527AF0F}resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{19803E27-A2F2-4517-A7B0-73EDF239B4D6}resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1991A0AF-7D17-4493-B5B2-1DCED6DB502E}resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1F2A7A89-9342-457F-BF2A-C26D1A01A2E4}resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=bool{26CF9E8D-7B3D-4828-9ABC-788302291FBE}resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=bool{27335150-337C-458F-AEDF-BDF23C6820CC}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=bool{29555C81-E676-4708-B543-E8C48A518EB3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2AB3B169-20FC-4443-BE33-975E88F509B8}resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=bool{2E7911A3-ACDC-4265-A2CC-02DF0A0A590D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{33A4CA04-47E3-4DF7-BD69-5E12C1DBDD4C}resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=bool{35534149-09DE-4991-A46F-A239DB71862B}resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=bool{366B01FF-7B7A-4B4D-A8EF-A1D853A5965B}resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=bool{370EE507-4F8F-416A-A0BE-1D3395932C93}resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=bool{374BD079-EA0B-414D-8C4A-986E34C79C0E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{37990431-8C69-4CCA-AB20-4037E80322F0}resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=bool{3BD18584-A6E5-469E-8948-BF5DCDBCB8E3}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3BDAD3AA-D7D6-45FD-8782-47F37B3991F8}resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3C9EC768-98CD-43AB-BD38-9FDFBCB250BB}resource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=bool{3F34FA8A-C19A-40AB-9D17-555D3362344C}resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=bool{4284D3DA-9AC5-419B-8045-949A5E8309AE}resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{428D6779-9889-4B8B-8B9E-AD5629546966}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]{43F0DCEF-21A6-43D8-B966-7B7A896719CE}resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=bool{45D31DD5-FE33-4FC2-A0C8-4CCDD441D2F1}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{45D6C4BC-DC92-44B4-B4D0-618C1DC0314B}resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=bool{479320EC-1DE5-4647-8815-6AC8A065ABBE}resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{501E965D-7E2A-4383-8C8F-5505C5BD214D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{599FBF48-F32D-4A7F-93FD-388CC4220CBA}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{5BF6E3AF-2B65-4DBD-8E08-59B5165C55AA}resource=/Scan Clock;0;ReadMethodType=bool{618E4B9F-A4BA-4CB7-96BC-87EB42C9A8EE}resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6243E5D0-79DF-402F-8A50-B53C5FF5DA98}resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6573F436-992A-4186-B93C-25FA36110953}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{65C9BFE9-D076-4016-8F33-B877F514E4BA}resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=bool{6A4593D3-0BC7-43B4-ADEF-4B9AFE2A709E}resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B3803FA-21DB-4AB6-A8CE-08CACD159105}resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=bool{6DB6A415-F34B-462D-9003-6463AF47A1D6}resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{72420D18-2378-4059-9177-FF96673B6B3A}Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{73B76717-5572-4802-BA31-503777B1510A}resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{764E07AA-36CB-4546-AE7A-B4C9359771A7}resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=bool{78A119D7-C6D2-457D-94E0-A2AD0BDF5E6C}resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7C6BB4F9-44AA-417E-A0EE-872BA2D274CB}resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7DA2B1F7-3BC3-40A5-B33F-A03CD0422223}resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{829CF660-10F4-4789-90C8-2B7EFCFE9A96}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{83335CFE-F24B-40B0-B52B-CC9F69E098DC}resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{84044843-3058-46A9-BD66-9040DB5219F1}resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=bool{8798E7B0-DC62-47F1-9C4C-115A83AD297C}resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=bool{89CC0C6F-61D1-4E0D-9741-7E896879729B}resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=bool{8BDF3502-5B80-484D-8EBF-475C5A4EDD59}resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32{8E2E6BFA-C292-429C-9034-045BF394DD75}resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E6501CE-C594-4F23-BBA3-F3FD1966EBD5}resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E808319-F342-494F-A546-04B7E6AD4F86}resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=bool{90493C34-764B-4DB9-9129-51515ADCB013}resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{909E9819-D2D0-4338-B59B-F4E6AA7039AB}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=bool{92D4FB2E-05BA-42BF-AA64-07D1311098C8}resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=bool{96409551-3F36-4634-8005-4AEB474E3CD9}resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=bool{96C3EFA7-E861-4EBF-83C9-E1AF218AACD3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=bool{99AF8B4F-77C8-42A9-81CB-7D06FEAC4C51}resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=bool{9D16FDC3-761A-47D3-9F16-090952B9DF93}[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{9EDBCFC2-0B59-4D94-8A86-8842917C4B36}cRIO Subresource{9F1D659C-6CEA-44F5-9F02-3D38B7292B84}resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A0733BDF-EDA7-44AC-82CD-EB0ED2694CCB}resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=bool{A2C409CD-9FA7-4207-A56D-7C70D1D6C429}resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=bool{A38E207B-95D1-44DD-858A-AAA53C2A2EE4}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=bool{A49ECA35-5C15-4599-9653-91D633E5E820}resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A4A959E2-EC1C-446A-91F8-E5F0DF041ADE}resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8{A98FB67C-6BBA-4376-A250-0114C7CFF5D5}resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A9D076F8-7203-4469-B2FE-91CE79B5BC88}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=bool{AAF52CFC-2B95-4D50-8BDB-4BAC04201C91}resource=/Chassis Temperature;0;ReadMethodType=i16{AB774714-8D35-40BF-86F8-066E6A8F1028}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{AF2673CD-BFDB-4489-B4AC-7610A070DEE7}resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3198168-FD4B-4BF0-BE27-D33A931E8124}resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=bool{B542CA3B-83BF-43AC-8C1E-39492FC065E0}resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=bool{B7DF432F-881A-46F1-AE34-74783A6DB980}resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BF3371D2-8AC7-4BA1-8135-8CE8C7834BBF}resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C160F4A5-A557-4D2C-94A5-50EA951DD088}resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C331EFF8-513C-415C-B62D-EFEE17754B24}cRIO Subresource{C3ABDF9D-A89E-4561-8B64-A6D08724CF8A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3B8FA71-2860-49DE-938E-1A72CA9B5295}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=bool{C4C096EC-CC95-4A09-813E-AD1BAAAD49FA}resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=bool{C79FB7B5-0CA6-4A63-97B3-1C2DE6AB6E90}resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=bool{C9377557-BDF5-4710-A0B0-D6DC4DFD122B}Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{D3C8C9C9-130D-4791-B2FB-35F6F161F90F}resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D5142CB3-57CC-417D-B6FF-98FBB258185F}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D8333EDB-840D-4440-B9CE-CE31080408D5}resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=bool{DDF6525D-4A0E-4266-BD58-C08100C45FA6}resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{DF8D857A-0EAE-4089-961A-DF6A77009713}resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E234C37E-7F8C-4EBE-ACD6-590F5A61C92F}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool{E234CAD5-52C0-450E-BF93-AF6EB6AC105A}resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8{E51654C2-E6B6-4D34-AD50-410968522D52}resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E7B8FD1D-E793-4F6C-B4D6-B8EF3138DE11}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=bool{E82C1336-89E1-46D4-8224-87CD2735A98A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{E9029A03-9269-478F-B190-93BE4A712ECE}resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=bool{EA023CC2-6328-48A6-8778-6B014504B20D}resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8{ED7CA35A-CBC7-47A9-8C6D-51FC68881467}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8{EE4DA3E6-24E0-474B-805C-ADD2FD81CC80}resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F1F2DD15-226C-415E-9382-888C2ACDC637}resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F8863AE3-7073-45C0-B117-9D7D60315F29}resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=bool{FE524C91-056A-415B-BB9A-58065C304E75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
						<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]cRIOAnalogInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]cRIOAnalogOutput[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIODigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]cRIODigitalOutput[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIOHsDigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOHsDigitalOutput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOSsdModul[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]DeviceCalibrationActual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsActual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsFifo"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"Encoder 1 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=boolEncoder 1 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=boolEncoder 2 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=boolFPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1/AI0resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI10resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI11resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI12resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI13resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI14resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI15resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI16resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI17resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI18resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI19resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI1resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI20resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI21resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI22resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI23resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI24resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI25resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI26resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI27resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI28resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI29resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI2resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI30resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI31resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI3resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI4resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI5resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI6resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI7resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI8resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI9resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/DI0resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=boolMod1/DO0resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=boolMod1/Trigresource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=boolMod3/DI1NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=boolMod3/DI2NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=boolMod3/DI4NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=boolMod3/DI5:0NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8Mod3/DI5NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=boolMod4/DO7:0ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DI0resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=boolMod5/DI10resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=boolMod5/DI11resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=boolMod5/DI12resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=boolMod5/DI13resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=boolMod5/DI14resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=boolMod5/DI15:8resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8Mod5/DI15resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=boolMod5/DI16resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=boolMod5/DI17resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=boolMod5/DI18resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=boolMod5/DI19resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=boolMod5/DI1resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=boolMod5/DI20resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=boolMod5/DI21resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=boolMod5/DI22resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=boolMod5/DI23:16resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8Mod5/DI23resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=boolMod5/DI24resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=boolMod5/DI25resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=boolMod5/DI26resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=boolMod5/DI27resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=boolMod5/DI28resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=boolMod5/DI29resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=boolMod5/DI2resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=boolMod5/DI30resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=boolMod5/DI31:0resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32Mod5/DI31:24resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8Mod5/DI31resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=boolMod5/DI3resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=boolMod5/DI4resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=boolMod5/DI5resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=boolMod5/DI6resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=boolMod5/DI7:0resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8Mod5/DI7resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=boolMod5/DI8resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=boolMod5/DI9resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSD Card 0cRIO SubresourceSD Card 1cRIO SubresourceSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSsiSynch1Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0SsiSynch2Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0System Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolTrig 1ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=boolTrig 2ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool</Property>
					</Item>
				</Item>
				<Item Name="VI" Type="Folder">
					<Item Name="Main.vi" Type="VI" URL="../Targets/cRIO9114/Main.vi">
						<Property Name="BuildSpec" Type="Str">{B6AE0665-5CAF-4E90-B127-D15148A3565E}</Property>
						<Property Name="configString.guid" Type="Str">{0073020B-2D9F-4450-8A6D-FB85A9636099}resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=bool{02D7301B-E6A1-4790-AC7E-B006C2CF5330}resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=bool{03B06DD0-C38E-40B6-B789-2E7364394881}resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=bool{047B03B7-47D4-4337-9691-DDE46857195C}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=bool{059C4215-D03A-439D-8C82-FFCB009DE0A2}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=bool{071122DE-3456-4D34-A2D3-28C6CD377950}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=bool{080F1513-66C2-421C-BFD9-567E9E5D3E1A}resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{0AE0D5EB-8DE3-4DDD-A2F1-7D02906FB4FC}resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8{0B0FD5A8-C8D0-4E34-AB64-10E9EDF189E7}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=bool{113EA66B-7BEB-4F5A-9BDC-A24733C2A8F1}resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=bool{120D309E-6AA7-429C-BAD9-42306571CCA2}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1640CC45-486E-42ED-A03C-88D12527AF0F}resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{19803E27-A2F2-4517-A7B0-73EDF239B4D6}resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1991A0AF-7D17-4493-B5B2-1DCED6DB502E}resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1F2A7A89-9342-457F-BF2A-C26D1A01A2E4}resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=bool{26CF9E8D-7B3D-4828-9ABC-788302291FBE}resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=bool{27335150-337C-458F-AEDF-BDF23C6820CC}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=bool{29555C81-E676-4708-B543-E8C48A518EB3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2AB3B169-20FC-4443-BE33-975E88F509B8}resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=bool{2E7911A3-ACDC-4265-A2CC-02DF0A0A590D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{33A4CA04-47E3-4DF7-BD69-5E12C1DBDD4C}resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=bool{35534149-09DE-4991-A46F-A239DB71862B}resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=bool{366B01FF-7B7A-4B4D-A8EF-A1D853A5965B}resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=bool{370EE507-4F8F-416A-A0BE-1D3395932C93}resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=bool{374BD079-EA0B-414D-8C4A-986E34C79C0E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{37990431-8C69-4CCA-AB20-4037E80322F0}resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=bool{3BD18584-A6E5-469E-8948-BF5DCDBCB8E3}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3BDAD3AA-D7D6-45FD-8782-47F37B3991F8}resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3C9EC768-98CD-43AB-BD38-9FDFBCB250BB}resource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=bool{3F34FA8A-C19A-40AB-9D17-555D3362344C}resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=bool{4284D3DA-9AC5-419B-8045-949A5E8309AE}resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{428D6779-9889-4B8B-8B9E-AD5629546966}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]{43F0DCEF-21A6-43D8-B966-7B7A896719CE}resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=bool{45D31DD5-FE33-4FC2-A0C8-4CCDD441D2F1}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{45D6C4BC-DC92-44B4-B4D0-618C1DC0314B}resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=bool{479320EC-1DE5-4647-8815-6AC8A065ABBE}resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{501E965D-7E2A-4383-8C8F-5505C5BD214D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{599FBF48-F32D-4A7F-93FD-388CC4220CBA}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{5BF6E3AF-2B65-4DBD-8E08-59B5165C55AA}resource=/Scan Clock;0;ReadMethodType=bool{618E4B9F-A4BA-4CB7-96BC-87EB42C9A8EE}resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6243E5D0-79DF-402F-8A50-B53C5FF5DA98}resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6573F436-992A-4186-B93C-25FA36110953}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{65C9BFE9-D076-4016-8F33-B877F514E4BA}resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=bool{6A4593D3-0BC7-43B4-ADEF-4B9AFE2A709E}resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B3803FA-21DB-4AB6-A8CE-08CACD159105}resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=bool{6DB6A415-F34B-462D-9003-6463AF47A1D6}resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{72420D18-2378-4059-9177-FF96673B6B3A}Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{73B76717-5572-4802-BA31-503777B1510A}resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{764E07AA-36CB-4546-AE7A-B4C9359771A7}resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=bool{78A119D7-C6D2-457D-94E0-A2AD0BDF5E6C}resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7C6BB4F9-44AA-417E-A0EE-872BA2D274CB}resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7DA2B1F7-3BC3-40A5-B33F-A03CD0422223}resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{829CF660-10F4-4789-90C8-2B7EFCFE9A96}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{83335CFE-F24B-40B0-B52B-CC9F69E098DC}resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{84044843-3058-46A9-BD66-9040DB5219F1}resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=bool{8798E7B0-DC62-47F1-9C4C-115A83AD297C}resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=bool{89CC0C6F-61D1-4E0D-9741-7E896879729B}resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=bool{8BDF3502-5B80-484D-8EBF-475C5A4EDD59}resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32{8E2E6BFA-C292-429C-9034-045BF394DD75}resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E6501CE-C594-4F23-BBA3-F3FD1966EBD5}resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E808319-F342-494F-A546-04B7E6AD4F86}resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=bool{90493C34-764B-4DB9-9129-51515ADCB013}resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{909E9819-D2D0-4338-B59B-F4E6AA7039AB}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=bool{92D4FB2E-05BA-42BF-AA64-07D1311098C8}resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=bool{96409551-3F36-4634-8005-4AEB474E3CD9}resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=bool{96C3EFA7-E861-4EBF-83C9-E1AF218AACD3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=bool{99AF8B4F-77C8-42A9-81CB-7D06FEAC4C51}resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=bool{9D16FDC3-761A-47D3-9F16-090952B9DF93}[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{9EDBCFC2-0B59-4D94-8A86-8842917C4B36}cRIO Subresource{9F1D659C-6CEA-44F5-9F02-3D38B7292B84}resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A0733BDF-EDA7-44AC-82CD-EB0ED2694CCB}resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=bool{A2C409CD-9FA7-4207-A56D-7C70D1D6C429}resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=bool{A38E207B-95D1-44DD-858A-AAA53C2A2EE4}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=bool{A49ECA35-5C15-4599-9653-91D633E5E820}resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A4A959E2-EC1C-446A-91F8-E5F0DF041ADE}resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8{A98FB67C-6BBA-4376-A250-0114C7CFF5D5}resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A9D076F8-7203-4469-B2FE-91CE79B5BC88}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=bool{AAF52CFC-2B95-4D50-8BDB-4BAC04201C91}resource=/Chassis Temperature;0;ReadMethodType=i16{AB774714-8D35-40BF-86F8-066E6A8F1028}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{AF2673CD-BFDB-4489-B4AC-7610A070DEE7}resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3198168-FD4B-4BF0-BE27-D33A931E8124}resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=bool{B542CA3B-83BF-43AC-8C1E-39492FC065E0}resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=bool{B7DF432F-881A-46F1-AE34-74783A6DB980}resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BF3371D2-8AC7-4BA1-8135-8CE8C7834BBF}resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C160F4A5-A557-4D2C-94A5-50EA951DD088}resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C331EFF8-513C-415C-B62D-EFEE17754B24}cRIO Subresource{C3ABDF9D-A89E-4561-8B64-A6D08724CF8A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3B8FA71-2860-49DE-938E-1A72CA9B5295}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=bool{C4C096EC-CC95-4A09-813E-AD1BAAAD49FA}resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=bool{C79FB7B5-0CA6-4A63-97B3-1C2DE6AB6E90}resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=bool{C9377557-BDF5-4710-A0B0-D6DC4DFD122B}Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{D3C8C9C9-130D-4791-B2FB-35F6F161F90F}resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D5142CB3-57CC-417D-B6FF-98FBB258185F}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D8333EDB-840D-4440-B9CE-CE31080408D5}resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=bool{DDF6525D-4A0E-4266-BD58-C08100C45FA6}resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{DF8D857A-0EAE-4089-961A-DF6A77009713}resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E234C37E-7F8C-4EBE-ACD6-590F5A61C92F}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool{E234CAD5-52C0-450E-BF93-AF6EB6AC105A}resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8{E51654C2-E6B6-4D34-AD50-410968522D52}resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E7B8FD1D-E793-4F6C-B4D6-B8EF3138DE11}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=bool{E82C1336-89E1-46D4-8224-87CD2735A98A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{E9029A03-9269-478F-B190-93BE4A712ECE}resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=bool{EA023CC2-6328-48A6-8778-6B014504B20D}resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8{ED7CA35A-CBC7-47A9-8C6D-51FC68881467}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8{EE4DA3E6-24E0-474B-805C-ADD2FD81CC80}resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F1F2DD15-226C-415E-9382-888C2ACDC637}resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F8863AE3-7073-45C0-B117-9D7D60315F29}resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=bool{FE524C91-056A-415B-BB9A-58065C304E75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
						<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]cRIOAnalogInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]cRIOAnalogOutput[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIODigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]cRIODigitalOutput[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIOHsDigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOHsDigitalOutput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOSsdModul[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]DeviceCalibrationActual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsActual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsFifo"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"Encoder 1 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=boolEncoder 1 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=boolEncoder 2 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=boolFPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1/AI0resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI10resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI11resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI12resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI13resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI14resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI15resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI16resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI17resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI18resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI19resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI1resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI20resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI21resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI22resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI23resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI24resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI25resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI26resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI27resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI28resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI29resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI2resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI30resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI31resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI3resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI4resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI5resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI6resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI7resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI8resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI9resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/DI0resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=boolMod1/DO0resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=boolMod1/Trigresource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=boolMod3/DI1NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=boolMod3/DI2NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=boolMod3/DI4NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=boolMod3/DI5:0NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8Mod3/DI5NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=boolMod4/DO7:0ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DI0resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=boolMod5/DI10resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=boolMod5/DI11resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=boolMod5/DI12resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=boolMod5/DI13resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=boolMod5/DI14resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=boolMod5/DI15:8resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8Mod5/DI15resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=boolMod5/DI16resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=boolMod5/DI17resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=boolMod5/DI18resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=boolMod5/DI19resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=boolMod5/DI1resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=boolMod5/DI20resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=boolMod5/DI21resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=boolMod5/DI22resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=boolMod5/DI23:16resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8Mod5/DI23resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=boolMod5/DI24resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=boolMod5/DI25resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=boolMod5/DI26resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=boolMod5/DI27resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=boolMod5/DI28resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=boolMod5/DI29resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=boolMod5/DI2resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=boolMod5/DI30resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=boolMod5/DI31:0resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32Mod5/DI31:24resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8Mod5/DI31resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=boolMod5/DI3resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=boolMod5/DI4resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=boolMod5/DI5resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=boolMod5/DI6resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=boolMod5/DI7:0resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8Mod5/DI7resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=boolMod5/DI8resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=boolMod5/DI9resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSD Card 0cRIO SubresourceSD Card 1cRIO SubresourceSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSsiSynch1Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0SsiSynch2Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0System Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolTrig 1ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=boolTrig 2ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool</Property>
						<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">C:\sources\LabVIEW\DVel\projecttemplate_ioengine\project\SourceCode\FPGA Bitfiles\Main.lvbitx</Property>
					</Item>
					<Item Name="InitDO.vi" Type="VI" URL="../FPGACode/InitDO.vi">
						<Property Name="configString.guid" Type="Str">{0073020B-2D9F-4450-8A6D-FB85A9636099}resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=bool{02D7301B-E6A1-4790-AC7E-B006C2CF5330}resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=bool{03B06DD0-C38E-40B6-B789-2E7364394881}resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=bool{047B03B7-47D4-4337-9691-DDE46857195C}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=bool{059C4215-D03A-439D-8C82-FFCB009DE0A2}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=bool{071122DE-3456-4D34-A2D3-28C6CD377950}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=bool{080F1513-66C2-421C-BFD9-567E9E5D3E1A}resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{0AE0D5EB-8DE3-4DDD-A2F1-7D02906FB4FC}resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8{0B0FD5A8-C8D0-4E34-AB64-10E9EDF189E7}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=bool{113EA66B-7BEB-4F5A-9BDC-A24733C2A8F1}resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=bool{120D309E-6AA7-429C-BAD9-42306571CCA2}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=bool{1640CC45-486E-42ED-A03C-88D12527AF0F}resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{19803E27-A2F2-4517-A7B0-73EDF239B4D6}resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1991A0AF-7D17-4493-B5B2-1DCED6DB502E}resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{1F2A7A89-9342-457F-BF2A-C26D1A01A2E4}resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=bool{26CF9E8D-7B3D-4828-9ABC-788302291FBE}resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=bool{27335150-337C-458F-AEDF-BDF23C6820CC}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=bool{29555C81-E676-4708-B543-E8C48A518EB3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=bool{2AB3B169-20FC-4443-BE33-975E88F509B8}resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=bool{2E7911A3-ACDC-4265-A2CC-02DF0A0A590D}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E{33A4CA04-47E3-4DF7-BD69-5E12C1DBDD4C}resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=bool{35534149-09DE-4991-A46F-A239DB71862B}resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=bool{366B01FF-7B7A-4B4D-A8EF-A1D853A5965B}resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=bool{370EE507-4F8F-416A-A0BE-1D3395932C93}resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=bool{374BD079-EA0B-414D-8C4A-986E34C79C0E}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]{37990431-8C69-4CCA-AB20-4037E80322F0}resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=bool{3BD18584-A6E5-469E-8948-BF5DCDBCB8E3}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=bool{3BDAD3AA-D7D6-45FD-8782-47F37B3991F8}resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{3C9EC768-98CD-43AB-BD38-9FDFBCB250BB}resource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=bool{3F34FA8A-C19A-40AB-9D17-555D3362344C}resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=bool{4284D3DA-9AC5-419B-8045-949A5E8309AE}resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{428D6779-9889-4B8B-8B9E-AD5629546966}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]{43F0DCEF-21A6-43D8-B966-7B7A896719CE}resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=bool{45D31DD5-FE33-4FC2-A0C8-4CCDD441D2F1}[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{45D6C4BC-DC92-44B4-B4D0-618C1DC0314B}resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=bool{479320EC-1DE5-4647-8815-6AC8A065ABBE}resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{501E965D-7E2A-4383-8C8F-5505C5BD214D}resource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{599FBF48-F32D-4A7F-93FD-388CC4220CBA}"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"{5BF6E3AF-2B65-4DBD-8E08-59B5165C55AA}resource=/Scan Clock;0;ReadMethodType=bool{618E4B9F-A4BA-4CB7-96BC-87EB42C9A8EE}resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6243E5D0-79DF-402F-8A50-B53C5FF5DA98}resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6573F436-992A-4186-B93C-25FA36110953}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{65C9BFE9-D076-4016-8F33-B877F514E4BA}resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=bool{6A4593D3-0BC7-43B4-ADEF-4B9AFE2A709E}resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{6B3803FA-21DB-4AB6-A8CE-08CACD159105}resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=bool{6DB6A415-F34B-462D-9003-6463AF47A1D6}resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{72420D18-2378-4059-9177-FF96673B6B3A}Actual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{73B76717-5572-4802-BA31-503777B1510A}resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{764E07AA-36CB-4546-AE7A-B4C9359771A7}resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=bool{78A119D7-C6D2-457D-94E0-A2AD0BDF5E6C}resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7C6BB4F9-44AA-417E-A0EE-872BA2D274CB}resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{7DA2B1F7-3BC3-40A5-B33F-A03CD0422223}resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{829CF660-10F4-4789-90C8-2B7EFCFE9A96}Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0{83335CFE-F24B-40B0-B52B-CC9F69E098DC}resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{84044843-3058-46A9-BD66-9040DB5219F1}resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=bool{8798E7B0-DC62-47F1-9C4C-115A83AD297C}resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=bool{89CC0C6F-61D1-4E0D-9741-7E896879729B}resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=bool{8BDF3502-5B80-484D-8EBF-475C5A4EDD59}resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32{8E2E6BFA-C292-429C-9034-045BF394DD75}resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E6501CE-C594-4F23-BBA3-F3FD1966EBD5}resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{8E808319-F342-494F-A546-04B7E6AD4F86}resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=bool{90493C34-764B-4DB9-9129-51515ADCB013}resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{909E9819-D2D0-4338-B59B-F4E6AA7039AB}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=bool{92D4FB2E-05BA-42BF-AA64-07D1311098C8}resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=bool{96409551-3F36-4634-8005-4AEB474E3CD9}resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=bool{96C3EFA7-E861-4EBF-83C9-E1AF218AACD3}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=bool{99AF8B4F-77C8-42A9-81CB-7D06FEAC4C51}resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=bool{9D16FDC3-761A-47D3-9F16-090952B9DF93}[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]{9EDBCFC2-0B59-4D94-8A86-8842917C4B36}cRIO Subresource{9F1D659C-6CEA-44F5-9F02-3D38B7292B84}resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A0733BDF-EDA7-44AC-82CD-EB0ED2694CCB}resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=bool{A2C409CD-9FA7-4207-A56D-7C70D1D6C429}resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=bool{A38E207B-95D1-44DD-858A-AAA53C2A2EE4}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=bool{A49ECA35-5C15-4599-9653-91D633E5E820}resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A4A959E2-EC1C-446A-91F8-E5F0DF041ADE}resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8{A98FB67C-6BBA-4376-A250-0114C7CFF5D5}resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{A9D076F8-7203-4469-B2FE-91CE79B5BC88}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=bool{AAF52CFC-2B95-4D50-8BDB-4BAC04201C91}resource=/Chassis Temperature;0;ReadMethodType=i16{AB774714-8D35-40BF-86F8-066E6A8F1028}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{AF2673CD-BFDB-4489-B4AC-7610A070DEE7}resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{B3198168-FD4B-4BF0-BE27-D33A931E8124}resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=bool{B542CA3B-83BF-43AC-8C1E-39492FC065E0}resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=bool{B7DF432F-881A-46F1-AE34-74783A6DB980}resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{BF3371D2-8AC7-4BA1-8135-8CE8C7834BBF}resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C160F4A5-A557-4D2C-94A5-50EA951DD088}resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{C331EFF8-513C-415C-B62D-EFEE17754B24}cRIO Subresource{C3ABDF9D-A89E-4561-8B64-A6D08724CF8A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{C3B8FA71-2860-49DE-938E-1A72CA9B5295}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=bool{C4C096EC-CC95-4A09-813E-AD1BAAAD49FA}resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=bool{C79FB7B5-0CA6-4A63-97B3-1C2DE6AB6E90}resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=bool{C9377557-BDF5-4710-A0B0-D6DC4DFD122B}Actual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1{D3C8C9C9-130D-4791-B2FB-35F6F161F90F}resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{D5142CB3-57CC-417D-B6FF-98FBB258185F}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{D8333EDB-840D-4440-B9CE-CE31080408D5}resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=bool{DDF6525D-4A0E-4266-BD58-C08100C45FA6}resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{DF8D857A-0EAE-4089-961A-DF6A77009713}resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E234C37E-7F8C-4EBE-ACD6-590F5A61C92F}ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool{E234CAD5-52C0-450E-BF93-AF6EB6AC105A}resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8{E51654C2-E6B6-4D34-AD50-410968522D52}resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{E7B8FD1D-E793-4F6C-B4D6-B8EF3138DE11}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=bool{E82C1336-89E1-46D4-8224-87CD2735A98A}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]{E9029A03-9269-478F-B190-93BE4A712ECE}resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=bool{EA023CC2-6328-48A6-8778-6B014504B20D}resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8{ED7CA35A-CBC7-47A9-8C6D-51FC68881467}NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8{EE4DA3E6-24E0-474B-805C-ADD2FD81CC80}resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F1F2DD15-226C-415E-9382-888C2ACDC637}resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctl{F8863AE3-7073-45C0-B117-9D7D60315F29}resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=bool{FE524C91-056A-415B-BB9A-58065C304E75}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
						<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427EChassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9114/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9114FPGA_TARGET_FAMILYVIRTEX5TARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]cRIOAnalogInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9205,cRIOModule.AI0.TerminalMode=0,cRIOModule.AI0.VoltageRange=0,cRIOModule.AI1.TerminalMode=0,cRIOModule.AI1.VoltageRange=0,cRIOModule.AI10.TerminalMode=0,cRIOModule.AI10.VoltageRange=0,cRIOModule.AI11.TerminalMode=0,cRIOModule.AI11.VoltageRange=0,cRIOModule.AI12.TerminalMode=0,cRIOModule.AI12.VoltageRange=0,cRIOModule.AI13.TerminalMode=0,cRIOModule.AI13.VoltageRange=0,cRIOModule.AI14.TerminalMode=0,cRIOModule.AI14.VoltageRange=0,cRIOModule.AI15.TerminalMode=0,cRIOModule.AI15.VoltageRange=0,cRIOModule.AI16.TerminalMode=0,cRIOModule.AI16.VoltageRange=0,cRIOModule.AI17.TerminalMode=0,cRIOModule.AI17.VoltageRange=0,cRIOModule.AI18.TerminalMode=0,cRIOModule.AI18.VoltageRange=0,cRIOModule.AI19.TerminalMode=0,cRIOModule.AI19.VoltageRange=0,cRIOModule.AI2.TerminalMode=0,cRIOModule.AI2.VoltageRange=0,cRIOModule.AI20.TerminalMode=0,cRIOModule.AI20.VoltageRange=0,cRIOModule.AI21.TerminalMode=0,cRIOModule.AI21.VoltageRange=0,cRIOModule.AI22.TerminalMode=0,cRIOModule.AI22.VoltageRange=0,cRIOModule.AI23.TerminalMode=0,cRIOModule.AI23.VoltageRange=0,cRIOModule.AI24.TerminalMode=0,cRIOModule.AI24.VoltageRange=0,cRIOModule.AI25.TerminalMode=0,cRIOModule.AI25.VoltageRange=0,cRIOModule.AI26.TerminalMode=0,cRIOModule.AI26.VoltageRange=0,cRIOModule.AI27.TerminalMode=0,cRIOModule.AI27.VoltageRange=0,cRIOModule.AI28.TerminalMode=0,cRIOModule.AI28.VoltageRange=0,cRIOModule.AI29.TerminalMode=0,cRIOModule.AI29.VoltageRange=0,cRIOModule.AI3.TerminalMode=0,cRIOModule.AI3.VoltageRange=0,cRIOModule.AI30.TerminalMode=0,cRIOModule.AI30.VoltageRange=0,cRIOModule.AI31.TerminalMode=0,cRIOModule.AI31.VoltageRange=0,cRIOModule.AI4.TerminalMode=0,cRIOModule.AI4.VoltageRange=0,cRIOModule.AI5.TerminalMode=0,cRIOModule.AI5.VoltageRange=0,cRIOModule.AI6.TerminalMode=0,cRIOModule.AI6.VoltageRange=0,cRIOModule.AI7.TerminalMode=0,cRIOModule.AI7.VoltageRange=0,cRIOModule.AI8.TerminalMode=0,cRIOModule.AI8.VoltageRange=0,cRIOModule.AI9.TerminalMode=0,cRIOModule.AI9.VoltageRange=0,cRIOModule.EnableCalProperties=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.MinConvTime=8,000000E+0,cRIOModule.RsiAttributes=[crioConfig.End]cRIOAnalogOutput[crioConfig.Begin]crio.Location=Slot 2,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIODigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 5,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]cRIODigitalOutput[crioConfig.Begin]crio.Location=Slot 6,cRIOModule.EnableHsInput=false,cRIOModule.EnableHsOutput=false,cRIOModule.EnableSpecialtyDigital=false[crioConfig.End]cRIOHsDigitalInput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9411,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOHsDigitalOutput[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 4,crio.Type=NI 9474,cRIOModule.DIO3_0InitialDir=0,cRIOModule.DIO7_4InitialDir=0,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.NumSyncRegs=11111111,cRIOModule.RsiAttributes=[crioConfig.End]cRIOSsdModul[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 7,crio.Type=NI 9802[crioConfig.End]DeviceCalibrationActual Number of Elements=132;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=1B80C7897A13A8462FDEE4B665A77BE1;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsActual Number of Elements=66;ReadArbs=0;WriteArbs=0;Implementation=2;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;InitDataHash=31B3F7A256BB6DDBC5C459AAD4953488;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=1DeviceMeasurementsFifo"ControlLogic=0;NumberOfElements=1023;Type=2;ReadArbs=Arbitrate if Multiple Requestors Only;ElementsPerRead=1;WriteArbs=Always Arbitrate;ElementsPerWrite=1;Implementation=2;DeviceMeasurementsFifo;DataType=1000800000000001003C005F03510020000000120001000100000012FFFFFFFFFFFFFFFF0000001F00000011000000007FFFFFFF00000001FFFFFFF3000000000000000100010000000000000000000000000000;DisableOnOverflowUnderflow=FALSE"Encoder 1 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO0;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO1;0;ReadMethodType=bool;WriteMethodType=boolEncoder 1 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI0;0;ReadMethodType=boolEncoder 1 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO2;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock +ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO4;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Clock -ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO5;0;ReadMethodType=bool;WriteMethodType=boolEncoder 2 Data +NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI3;0;ReadMethodType=boolEncoder 2 ZeroArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO3;0;ReadMethodType=bool;WriteMethodType=boolFPGA LEDresource=/FPGA LED;0;ReadMethodType=bool;WriteMethodType=boolMod1/AI0resource=/crio_cRIOAnalogInput/AI0;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI10resource=/crio_cRIOAnalogInput/AI10;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI11resource=/crio_cRIOAnalogInput/AI11;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI12resource=/crio_cRIOAnalogInput/AI12;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI13resource=/crio_cRIOAnalogInput/AI13;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI14resource=/crio_cRIOAnalogInput/AI14;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI15resource=/crio_cRIOAnalogInput/AI15;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI16resource=/crio_cRIOAnalogInput/AI16;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI17resource=/crio_cRIOAnalogInput/AI17;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI18resource=/crio_cRIOAnalogInput/AI18;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI19resource=/crio_cRIOAnalogInput/AI19;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI1resource=/crio_cRIOAnalogInput/AI1;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI20resource=/crio_cRIOAnalogInput/AI20;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI21resource=/crio_cRIOAnalogInput/AI21;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI22resource=/crio_cRIOAnalogInput/AI22;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI23resource=/crio_cRIOAnalogInput/AI23;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI24resource=/crio_cRIOAnalogInput/AI24;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI25resource=/crio_cRIOAnalogInput/AI25;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI26resource=/crio_cRIOAnalogInput/AI26;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI27resource=/crio_cRIOAnalogInput/AI27;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI28resource=/crio_cRIOAnalogInput/AI28;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI29resource=/crio_cRIOAnalogInput/AI29;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI2resource=/crio_cRIOAnalogInput/AI2;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI30resource=/crio_cRIOAnalogInput/AI30;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI31resource=/crio_cRIOAnalogInput/AI31;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI3resource=/crio_cRIOAnalogInput/AI3;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI4resource=/crio_cRIOAnalogInput/AI4;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI5resource=/crio_cRIOAnalogInput/AI5;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI6resource=/crio_cRIOAnalogInput/AI6;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI7resource=/crio_cRIOAnalogInput/AI7;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI8resource=/crio_cRIOAnalogInput/AI8;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/AI9resource=/crio_cRIOAnalogInput/AI9;0;ReadMethodType=vi.lib\LabVIEW Targets\FPGA\cRIO\shared\nicrio_FXP_Controls\nicrio_FXP_S_26_5.ctlMod1/DI0resource=/crio_cRIOAnalogInput/DI0;0;ReadMethodType=boolMod1/DO0resource=/crio_cRIOAnalogInput/DO0;0;WriteMethodType=boolMod1/Trigresource=/crio_cRIOAnalogInput/Trig;0;ReadMethodType=boolMod3/DI1NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI1;0;ReadMethodType=boolMod3/DI2NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI2;0;ReadMethodType=boolMod3/DI4NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI4;0;ReadMethodType=boolMod3/DI5:0NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5:0;0;ReadMethodType=u8Mod3/DI5NumberOfSyncRegistersForReadInProject=Auto;resource=/crio_CRioHsDigitalInput/DI5;0;ReadMethodType=boolMod4/DO7:0ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod5/DI0resource=/crio_cRIODigitalInput/DI0;0;ReadMethodType=boolMod5/DI10resource=/crio_cRIODigitalInput/DI10;0;ReadMethodType=boolMod5/DI11resource=/crio_cRIODigitalInput/DI11;0;ReadMethodType=boolMod5/DI12resource=/crio_cRIODigitalInput/DI12;0;ReadMethodType=boolMod5/DI13resource=/crio_cRIODigitalInput/DI13;0;ReadMethodType=boolMod5/DI14resource=/crio_cRIODigitalInput/DI14;0;ReadMethodType=boolMod5/DI15:8resource=/crio_cRIODigitalInput/DI15:8;0;ReadMethodType=u8Mod5/DI15resource=/crio_cRIODigitalInput/DI15;0;ReadMethodType=boolMod5/DI16resource=/crio_cRIODigitalInput/DI16;0;ReadMethodType=boolMod5/DI17resource=/crio_cRIODigitalInput/DI17;0;ReadMethodType=boolMod5/DI18resource=/crio_cRIODigitalInput/DI18;0;ReadMethodType=boolMod5/DI19resource=/crio_cRIODigitalInput/DI19;0;ReadMethodType=boolMod5/DI1resource=/crio_cRIODigitalInput/DI1;0;ReadMethodType=boolMod5/DI20resource=/crio_cRIODigitalInput/DI20;0;ReadMethodType=boolMod5/DI21resource=/crio_cRIODigitalInput/DI21;0;ReadMethodType=boolMod5/DI22resource=/crio_cRIODigitalInput/DI22;0;ReadMethodType=boolMod5/DI23:16resource=/crio_cRIODigitalInput/DI23:16;0;ReadMethodType=u8Mod5/DI23resource=/crio_cRIODigitalInput/DI23;0;ReadMethodType=boolMod5/DI24resource=/crio_cRIODigitalInput/DI24;0;ReadMethodType=boolMod5/DI25resource=/crio_cRIODigitalInput/DI25;0;ReadMethodType=boolMod5/DI26resource=/crio_cRIODigitalInput/DI26;0;ReadMethodType=boolMod5/DI27resource=/crio_cRIODigitalInput/DI27;0;ReadMethodType=boolMod5/DI28resource=/crio_cRIODigitalInput/DI28;0;ReadMethodType=boolMod5/DI29resource=/crio_cRIODigitalInput/DI29;0;ReadMethodType=boolMod5/DI2resource=/crio_cRIODigitalInput/DI2;0;ReadMethodType=boolMod5/DI30resource=/crio_cRIODigitalInput/DI30;0;ReadMethodType=boolMod5/DI31:0resource=/crio_cRIODigitalInput/DI31:0;0;ReadMethodType=u32Mod5/DI31:24resource=/crio_cRIODigitalInput/DI31:24;0;ReadMethodType=u8Mod5/DI31resource=/crio_cRIODigitalInput/DI31;0;ReadMethodType=boolMod5/DI3resource=/crio_cRIODigitalInput/DI3;0;ReadMethodType=boolMod5/DI4resource=/crio_cRIODigitalInput/DI4;0;ReadMethodType=boolMod5/DI5resource=/crio_cRIODigitalInput/DI5;0;ReadMethodType=boolMod5/DI6resource=/crio_cRIODigitalInput/DI6;0;ReadMethodType=boolMod5/DI7:0resource=/crio_cRIODigitalInput/DI7:0;0;ReadMethodType=u8Mod5/DI7resource=/crio_cRIODigitalInput/DI7;0;ReadMethodType=boolMod5/DI8resource=/crio_cRIODigitalInput/DI8;0;ReadMethodType=boolMod5/DI9resource=/crio_cRIODigitalInput/DI9;0;ReadMethodType=boolScan Clockresource=/Scan Clock;0;ReadMethodType=boolSD Card 0cRIO SubresourceSD Card 1cRIO SubresourceSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSsiSynch1Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0SsiSynch2Actual Number of Elements=2;ReadArbs=2;WriteArbs=2;Implementation=1;DataType=1000800000000001000A402104426F6F6C00000100000000000000;InitDataHash=ABD1701A9E1F6AAB0E25C511CFC01EA8;DRAM Selection=;DRAM Max Outstanding Requests=32;DRAM Include Byte Enables=FALSE;DRAM Grant Time=50;Interface Configuration=Read A-Write B;Multiple Clock Domains=FALSE;Memory Latency=0System Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolTrig 1ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO6;0;ReadMethodType=bool;WriteMethodType=boolTrig 2ArbitrationForOutputData=NeverArbitrate;resource=/crio_CRioHsDigitalOutput/DO7;0;ReadMethodType=bool;WriteMethodType=bool</Property>
					</Item>
				</Item>
				<Item Name="Mod1" Type="Folder">
					<Item Name="cRIOAnalogInput" Type="RIO C Series Module">
						<Property Name="crio.Calibration" Type="Str">1</Property>
						<Property Name="crio.Location" Type="Str">Slot 1</Property>
						<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
						<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
						<Property Name="crio.Type" Type="Str">NI 9205</Property>
						<Property Name="cRIOModule.AI0.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI0.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI1.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI1.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI10.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI10.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI11.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI11.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI12.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI12.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI13.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI13.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI14.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI14.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI15.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI15.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI16.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI16.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI17.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI17.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI18.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI18.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI19.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI19.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI2.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI2.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI20.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI20.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI21.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI21.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI22.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI22.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI23.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI23.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI24.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI24.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI25.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI25.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI26.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI26.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI27.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI27.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI28.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI28.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI29.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI29.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI3.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI3.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI30.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI30.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI31.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI31.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI4.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI4.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI5.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI5.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI6.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI6.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI7.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI7.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI8.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI8.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.AI9.TerminalMode" Type="Str">0</Property>
						<Property Name="cRIOModule.AI9.VoltageRange" Type="Str">0</Property>
						<Property Name="cRIOModule.EnableCalProperties" Type="Str">false</Property>
						<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
						<Property Name="cRIOModule.MinConvTime" Type="Str">8,000000E+0</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{374BD079-EA0B-414D-8C4A-986E34C79C0E}</Property>
					</Item>
					<Item Name="Mod1/AI0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{73B76717-5572-4802-BA31-503777B1510A}</Property>
					</Item>
					<Item Name="Mod1/AI1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{618E4B9F-A4BA-4CB7-96BC-87EB42C9A8EE}</Property>
					</Item>
					<Item Name="Mod1/AI2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A49ECA35-5C15-4599-9653-91D633E5E820}</Property>
					</Item>
					<Item Name="Mod1/AI3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8E2E6BFA-C292-429C-9034-045BF394DD75}</Property>
					</Item>
					<Item Name="Mod1/AI4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BF3371D2-8AC7-4BA1-8135-8CE8C7834BBF}</Property>
					</Item>
					<Item Name="Mod1/AI5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{479320EC-1DE5-4647-8815-6AC8A065ABBE}</Property>
					</Item>
					<Item Name="Mod1/AI6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A98FB67C-6BBA-4376-A250-0114C7CFF5D5}</Property>
					</Item>
					<Item Name="Mod1/AI7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3BDAD3AA-D7D6-45FD-8782-47F37B3991F8}</Property>
					</Item>
					<Item Name="Mod1/AI8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7DA2B1F7-3BC3-40A5-B33F-A03CD0422223}</Property>
					</Item>
					<Item Name="Mod1/AI9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DF8D857A-0EAE-4089-961A-DF6A77009713}</Property>
					</Item>
					<Item Name="Mod1/AI10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B7DF432F-881A-46F1-AE34-74783A6DB980}</Property>
					</Item>
					<Item Name="Mod1/AI11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6243E5D0-79DF-402F-8A50-B53C5FF5DA98}</Property>
					</Item>
					<Item Name="Mod1/AI12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6DB6A415-F34B-462D-9003-6463AF47A1D6}</Property>
					</Item>
					<Item Name="Mod1/AI13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E51654C2-E6B6-4D34-AD50-410968522D52}</Property>
					</Item>
					<Item Name="Mod1/AI14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DDF6525D-4A0E-4266-BD58-C08100C45FA6}</Property>
					</Item>
					<Item Name="Mod1/AI15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F1F2DD15-226C-415E-9382-888C2ACDC637}</Property>
					</Item>
					<Item Name="Mod1/AI16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6A4593D3-0BC7-43B4-ADEF-4B9AFE2A709E}</Property>
					</Item>
					<Item Name="Mod1/AI17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{83335CFE-F24B-40B0-B52B-CC9F69E098DC}</Property>
					</Item>
					<Item Name="Mod1/AI18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D3C8C9C9-130D-4791-B2FB-35F6F161F90F}</Property>
					</Item>
					<Item Name="Mod1/AI19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1991A0AF-7D17-4493-B5B2-1DCED6DB502E}</Property>
					</Item>
					<Item Name="Mod1/AI20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{78A119D7-C6D2-457D-94E0-A2AD0BDF5E6C}</Property>
					</Item>
					<Item Name="Mod1/AI21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C160F4A5-A557-4D2C-94A5-50EA951DD088}</Property>
					</Item>
					<Item Name="Mod1/AI22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EE4DA3E6-24E0-474B-805C-ADD2FD81CC80}</Property>
					</Item>
					<Item Name="Mod1/AI23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1640CC45-486E-42ED-A03C-88D12527AF0F}</Property>
					</Item>
					<Item Name="Mod1/AI24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AF2673CD-BFDB-4489-B4AC-7610A070DEE7}</Property>
					</Item>
					<Item Name="Mod1/AI25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8E6501CE-C594-4F23-BBA3-F3FD1966EBD5}</Property>
					</Item>
					<Item Name="Mod1/AI26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{19803E27-A2F2-4517-A7B0-73EDF239B4D6}</Property>
					</Item>
					<Item Name="Mod1/AI27" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{080F1513-66C2-421C-BFD9-567E9E5D3E1A}</Property>
					</Item>
					<Item Name="Mod1/AI28" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9F1D659C-6CEA-44F5-9F02-3D38B7292B84}</Property>
					</Item>
					<Item Name="Mod1/AI29" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7C6BB4F9-44AA-417E-A0EE-872BA2D274CB}</Property>
					</Item>
					<Item Name="Mod1/AI30" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4284D3DA-9AC5-419B-8045-949A5E8309AE}</Property>
					</Item>
					<Item Name="Mod1/AI31" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/AI31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{90493C34-764B-4DB9-9129-51515ADCB013}</Property>
					</Item>
					<Item Name="Mod1/DI0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{45D6C4BC-DC92-44B4-B4D0-618C1DC0314B}</Property>
					</Item>
					<Item Name="Mod1/DO0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C79FB7B5-0CA6-4A63-97B3-1C2DE6AB6E90}</Property>
					</Item>
					<Item Name="Mod1/Trig" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIOAnalogInput/Trig</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3C9EC768-98CD-43AB-BD38-9FDFBCB250BB}</Property>
					</Item>
				</Item>
				<Item Name="Mod3" Type="Folder">
					<Item Name="cRIOHsDigitalInput" Type="RIO C Series Module">
						<Property Name="crio.Calibration" Type="Str">1</Property>
						<Property Name="crio.Location" Type="Str">Slot 3</Property>
						<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
						<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
						<Property Name="crio.Type" Type="Str">NI 9411</Property>
						<Property Name="cRIOModule.DIO3_0InitialDir" Type="Str">0</Property>
						<Property Name="cRIOModule.DIO7_4InitialDir" Type="Str">0</Property>
						<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
						<Property Name="cRIOModule.NumSyncRegs" Type="Str">11111111</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FE524C91-056A-415B-BB9A-58065C304E75}</Property>
					</Item>
					<Item Name="Encoder 1 Data +" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalInput/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{047B03B7-47D4-4337-9691-DDE46857195C}</Property>
					</Item>
					<Item Name="Mod3/DI1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalInput/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0B0FD5A8-C8D0-4E34-AB64-10E9EDF189E7}</Property>
					</Item>
					<Item Name="Encoder 2 Data +" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalInput/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{909E9819-D2D0-4338-B59B-F4E6AA7039AB}</Property>
					</Item>
					<Item Name="Mod3/DI4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalInput/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E7B8FD1D-E793-4F6C-B4D6-B8EF3138DE11}</Property>
					</Item>
					<Item Name="Mod3/DI2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalInput/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{059C4215-D03A-439D-8C82-FFCB009DE0A2}</Property>
					</Item>
					<Item Name="Mod3/DI5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalInput/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A38E207B-95D1-44DD-858A-AAA53C2A2EE4}</Property>
					</Item>
					<Item Name="Mod3/DI5:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalInput/DI5:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{ED7CA35A-CBC7-47A9-8C6D-51FC68881467}</Property>
					</Item>
				</Item>
				<Item Name="Mod4" Type="Folder">
					<Item Name="cRIOHsDigitalOutput" Type="RIO C Series Module">
						<Property Name="crio.Calibration" Type="Str">1</Property>
						<Property Name="crio.Location" Type="Str">Slot 4</Property>
						<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
						<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
						<Property Name="crio.Type" Type="Str">NI 9474</Property>
						<Property Name="cRIOModule.DIO3_0InitialDir" Type="Str">0</Property>
						<Property Name="cRIOModule.DIO7_4InitialDir" Type="Str">0</Property>
						<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
						<Property Name="cRIOModule.NumSyncRegs" Type="Str">11111111</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E82C1336-89E1-46D4-8224-87CD2735A98A}</Property>
					</Item>
					<Item Name="Encoder 1 Clock +" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C3B8FA71-2860-49DE-938E-1A72CA9B5295}</Property>
					</Item>
					<Item Name="Encoder 1 Clock -" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{071122DE-3456-4D34-A2D3-28C6CD377950}</Property>
					</Item>
					<Item Name="Encoder 1 Zero" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A9D076F8-7203-4469-B2FE-91CE79B5BC88}</Property>
					</Item>
					<Item Name="Encoder 2 Zero" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{27335150-337C-458F-AEDF-BDF23C6820CC}</Property>
					</Item>
					<Item Name="Encoder 2 Clock +" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{29555C81-E676-4708-B543-E8C48A518EB3}</Property>
					</Item>
					<Item Name="Encoder 2 Clock -" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{120D309E-6AA7-429C-BAD9-42306571CCA2}</Property>
					</Item>
					<Item Name="Trig 1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{96C3EFA7-E861-4EBF-83C9-E1AF218AACD3}</Property>
					</Item>
					<Item Name="Trig 2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E234C37E-7F8C-4EBE-ACD6-590F5A61C92F}</Property>
					</Item>
					<Item Name="Mod4/DO7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/crio_CRioHsDigitalOutput/DO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AB774714-8D35-40BF-86F8-066E6A8F1028}</Property>
					</Item>
				</Item>
				<Item Name="Mod5" Type="Folder">
					<Item Name="cRIODigitalInput" Type="RIO C Series Module">
						<Property Name="crio.Calibration" Type="Str">1</Property>
						<Property Name="crio.Location" Type="Str">Slot 5</Property>
						<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
						<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
						<Property Name="crio.Type" Type="Str">NI 9425</Property>
						<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C3ABDF9D-A89E-4561-8B64-A6D08724CF8A}</Property>
					</Item>
					<Item Name="Mod5/DI0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{37990431-8C69-4CCA-AB20-4037E80322F0}</Property>
					</Item>
					<Item Name="Mod5/DI1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{370EE507-4F8F-416A-A0BE-1D3395932C93}</Property>
					</Item>
					<Item Name="Mod5/DI2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{02D7301B-E6A1-4790-AC7E-B006C2CF5330}</Property>
					</Item>
					<Item Name="Mod5/DI3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{99AF8B4F-77C8-42A9-81CB-7D06FEAC4C51}</Property>
					</Item>
					<Item Name="Mod5/DI4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0073020B-2D9F-4450-8A6D-FB85A9636099}</Property>
					</Item>
					<Item Name="Mod5/DI5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D8333EDB-840D-4440-B9CE-CE31080408D5}</Property>
					</Item>
					<Item Name="Mod5/DI6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E9029A03-9269-478F-B190-93BE4A712ECE}</Property>
					</Item>
					<Item Name="Mod5/DI7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{03B06DD0-C38E-40B6-B789-2E7364394881}</Property>
					</Item>
					<Item Name="Mod5/DI7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E234CAD5-52C0-450E-BF93-AF6EB6AC105A}</Property>
					</Item>
					<Item Name="Mod5/DI8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6B3803FA-21DB-4AB6-A8CE-08CACD159105}</Property>
					</Item>
					<Item Name="Mod5/DI9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{43F0DCEF-21A6-43D8-B966-7B7A896719CE}</Property>
					</Item>
					<Item Name="Mod5/DI10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{764E07AA-36CB-4546-AE7A-B4C9359771A7}</Property>
					</Item>
					<Item Name="Mod5/DI11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{65C9BFE9-D076-4016-8F33-B877F514E4BA}</Property>
					</Item>
					<Item Name="Mod5/DI12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1F2A7A89-9342-457F-BF2A-C26D1A01A2E4}</Property>
					</Item>
					<Item Name="Mod5/DI13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C4C096EC-CC95-4A09-813E-AD1BAAAD49FA}</Property>
					</Item>
					<Item Name="Mod5/DI14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8E808319-F342-494F-A546-04B7E6AD4F86}</Property>
					</Item>
					<Item Name="Mod5/DI15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{89CC0C6F-61D1-4E0D-9741-7E896879729B}</Property>
					</Item>
					<Item Name="Mod5/DI15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A4A959E2-EC1C-446A-91F8-E5F0DF041ADE}</Property>
					</Item>
					<Item Name="Mod5/DI16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{96409551-3F36-4634-8005-4AEB474E3CD9}</Property>
					</Item>
					<Item Name="Mod5/DI17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{84044843-3058-46A9-BD66-9040DB5219F1}</Property>
					</Item>
					<Item Name="Mod5/DI18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A2C409CD-9FA7-4207-A56D-7C70D1D6C429}</Property>
					</Item>
					<Item Name="Mod5/DI19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8798E7B0-DC62-47F1-9C4C-115A83AD297C}</Property>
					</Item>
					<Item Name="Mod5/DI20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{366B01FF-7B7A-4B4D-A8EF-A1D853A5965B}</Property>
					</Item>
					<Item Name="Mod5/DI21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A0733BDF-EDA7-44AC-82CD-EB0ED2694CCB}</Property>
					</Item>
					<Item Name="Mod5/DI22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{35534149-09DE-4991-A46F-A239DB71862B}</Property>
					</Item>
					<Item Name="Mod5/DI23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{113EA66B-7BEB-4F5A-9BDC-A24733C2A8F1}</Property>
					</Item>
					<Item Name="Mod5/DI23:16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EA023CC2-6328-48A6-8778-6B014504B20D}</Property>
					</Item>
					<Item Name="Mod5/DI24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B542CA3B-83BF-43AC-8C1E-39492FC065E0}</Property>
					</Item>
					<Item Name="Mod5/DI25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B3198168-FD4B-4BF0-BE27-D33A931E8124}</Property>
					</Item>
					<Item Name="Mod5/DI26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{33A4CA04-47E3-4DF7-BD69-5E12C1DBDD4C}</Property>
					</Item>
					<Item Name="Mod5/DI27" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3F34FA8A-C19A-40AB-9D17-555D3362344C}</Property>
					</Item>
					<Item Name="Mod5/DI28" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{92D4FB2E-05BA-42BF-AA64-07D1311098C8}</Property>
					</Item>
					<Item Name="Mod5/DI29" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F8863AE3-7073-45C0-B117-9D7D60315F29}</Property>
					</Item>
					<Item Name="Mod5/DI30" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2AB3B169-20FC-4443-BE33-975E88F509B8}</Property>
					</Item>
					<Item Name="Mod5/DI31" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{26CF9E8D-7B3D-4828-9ABC-788302291FBE}</Property>
					</Item>
					<Item Name="Mod5/DI31:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8BDF3502-5B80-484D-8EBF-475C5A4EDD59}</Property>
					</Item>
					<Item Name="Mod5/DI31:24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_cRIODigitalInput/DI31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0AE0D5EB-8DE3-4DDD-A2F1-7D02906FB4FC}</Property>
					</Item>
				</Item>
				<Item Name="Chassis I/O" Type="Folder">
					<Item Name="Chassis Temperature" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Chassis Temperature</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AAF52CFC-2B95-4D50-8BDB-4BAC04201C91}</Property>
					</Item>
					<Item Name="FPGA LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/FPGA LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{501E965D-7E2A-4383-8C8F-5505C5BD214D}</Property>
					</Item>
					<Item Name="Scan Clock" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Scan Clock</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5BF6E3AF-2B65-4DBD-8E08-59B5165C55AA}</Property>
					</Item>
					<Item Name="Sleep" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Sleep</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3BD18584-A6E5-469E-8948-BF5DCDBCB8E3}</Property>
					</Item>
					<Item Name="System Reset" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/System Reset</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D5142CB3-57CC-417D-B6FF-98FBB258185F}</Property>
					</Item>
				</Item>
				<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
					<Property Name="FPGA.PersistentID" Type="Str">{2E7911A3-ACDC-4265-A2CC-02DF0A0A590D}</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
					<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				</Item>
				<Item Name="cRIOSsdModul" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 7</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9802</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{428D6779-9889-4B8B-8B9E-AD5629546966}</Property>
					<Item Name="SD Card 0" Type="RIO Subresource">
						<Property Name="FPGA.PersistentID" Type="Str">{9EDBCFC2-0B59-4D94-8A86-8842917C4B36}</Property>
					</Item>
					<Item Name="SD Card 1" Type="RIO Subresource">
						<Property Name="FPGA.PersistentID" Type="Str">{C331EFF8-513C-415C-B62D-EFEE17754B24}</Property>
					</Item>
				</Item>
				<Item Name="IP Builder" Type="IP Builder Target">
					<Item Name="Dependencies" Type="Dependencies"/>
					<Item Name="Build Specifications" Type="Build"/>
				</Item>
				<Item Name="Dependencies" Type="Dependencies">
					<Item Name="vi.lib" Type="Folder">
						<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="FxpSim.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/FXPMathLib/sim/FxpSim.dll"/>
						<Item Name="niFPGA BW CU Order 2 (32-bit).vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Analysis/measure/butterworth/templates/niFPGA BW CU Order 2 (32-bit).vi"/>
						<Item Name="niFPGA I32xI32 MAC - FXP.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Analysis/utilities/niFPGA I32xI32 MAC - FXP.vi"/>
						<Item Name="niFPGA I32xI32 MAC+ MSB.vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Analysis/utilities/niFPGA I32xI32 MAC+ MSB.vi"/>
						<Item Name="niFPGA Read Write Memory (I32).vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Analysis/utilities/niFPGA Read Write Memory (I32).vi"/>
						<Item Name="niFPGA BW CU Order 2  n-chan (16-bit).vi" Type="VI" URL="/&lt;vilib&gt;/rvi/Analysis/measure/butterworth/templates/niFPGA BW CU Order 2  n-chan (16-bit).vi"/>
						<Item Name="LVFixedPointOverflowPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointOverflowPolicyTypeDef.ctl"/>
						<Item Name="LVFixedPointQuantizationPolicyTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/fxp/LVFixedPointQuantizationPolicyTypeDef.ctl"/>
						<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					</Item>
				</Item>
				<Item Name="Build Specifications" Type="Build">
					<Item Name="Main" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">Main</Property>
						<Property Name="Comp.BitfileName" Type="Str">Main.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">18</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">timing</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">high</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">high</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="ProjectPath" Type="Path">/C/Git/projecttemplate_ioengine_lv2014/project/SourceCode/projecttemplate_ioengine_rt.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
						<Property Name="TargetName" Type="Str">FPGA ControlSystem</Property>
						<Property Name="TopLevelVI" Type="Ref">/ControlSystem/Chassis/FPGA ControlSystem/VI/Main.vi</Property>
					</Item>
					<Item Name="SsiClocker" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">SsiClocker</Property>
						<Property Name="Comp.BitfileName" Type="Str">labviewpilioengi_FPGAControlSyste_SsiClocker_831167E9.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">timing</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">high</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">high</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">/C/sources/labview/gitlab/projecttemplate_ioengine/project/Builds/FPGA</Property>
						<Property Name="ProjectPath" Type="Path">/C/Git/projecttemplate_ioengine_lv2014/project/SourceCode/projecttemplate_ioengine_rt.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">false</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
						<Property Name="TargetName" Type="Str">FPGA ControlSystem</Property>
						<Property Name="TopLevelVI" Type="Ref">/ControlSystem/Chassis/FPGA ControlSystem/Devices/Encoder/SsiClocker.vi</Property>
					</Item>
					<Item Name="TestClocking" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">TestClocking</Property>
						<Property Name="Comp.BitfileName" Type="Str">projecttemplatei_FPGAControlSyste_TestClocking_0E26D6F5.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str">Default</Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">timing</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">high(timing)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">high</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">high</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">/C/sources/labview/gitlab/projecttemplate_ioengine/project/Builds/FPGA</Property>
						<Property Name="ProjectPath" Type="Path">/C/Git/projecttemplate_ioengine_lv2014/project/SourceCode/projecttemplate_ioengine_rt.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">false</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">true</Property>
						<Property Name="TargetName" Type="Str">FPGA ControlSystem</Property>
						<Property Name="TopLevelVI" Type="Ref"></Property>
					</Item>
				</Item>
			</Item>
			<Item Name="cRIOAnalogOutput" Type="RIO C Series Module">
				<Property Name="crio.Calibration" Type="Str">1</Property>
				<Property Name="crio.Location" Type="Str">Slot 2</Property>
				<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
				<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
				<Property Name="crio.Type" Type="Str">NI 9264</Property>
				<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
				<Property Name="cRIOModule.HotSwapMode" Type="Str">0</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{9D16FDC3-761A-47D3-9F16-090952B9DF93}</Property>
				<Item Name="0 GeneratorSetSpeed_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO0</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="1 AnchorMotorSetSpeed_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO1</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="2 AnchorMotorSetTorque_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO2</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="3 DCDCConverterRamp_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO3</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="4 NC_140" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO4</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="5 NC_141" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO5</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="6 NC_142" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO6</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="7 NC_143" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO7</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="8 NC_144" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO8</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="9 NC_145" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO9</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="10 NC_146" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO10</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="11 NC_147" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO11</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="12 NC_148" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO12</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="13 NC_149" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO13</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="14 NC_150" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO14</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
				<Item Name="15 NC_151" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">AO15</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">(1!!!"1!A!!!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!</Property>
				</Item>
			</Item>
			<Item Name="cRIODigitalOutput" Type="RIO C Series Module">
				<Property Name="crio.Calibration" Type="Str">1</Property>
				<Property Name="crio.Location" Type="Str">Slot 6</Property>
				<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
				<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
				<Property Name="crio.Type" Type="Str">NI 9476</Property>
				<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
				<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
				<Property Name="FPGA.PersistentID" Type="Str">{45D31DD5-FE33-4FC2-A0C8-4CCDD441D2F1}</Property>
				<Item Name="0 GeneratorStart_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">0</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO0</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="1 GeneratorForward_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">1</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO1</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="2 NC_152" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">2</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO2</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="3 AnchorMotorStart_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">3</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO3</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="4 AnchorMotorForward_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">4</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO4</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="5 NC_153" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">5</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO5</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="6 ElectroHydraulicBrake_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">6</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO6</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="7 Clutch_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">7</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO7</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="8 BilgePump_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">8</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO8</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="9 BilgePump_102" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">9</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO9</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="10 BilgePump_103" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">10</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO10</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="11 BilgePump_104" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">11</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO11</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="12 BilgePump_105" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">12</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO12</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="13 BilgePump_106" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">13</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO13</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="14 NC_154" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">14</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO14</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="15 SeaWaterPump_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">15</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO15</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="16 CirculationWaterPump_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">16</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO16</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="17 WallasHeaterStart_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">17</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO17</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="18 CoolingFan_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">18</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO18</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="19 AnchorMotorBrake_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">19</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO19</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="20 NC_155" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">20</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO20</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="21 DCDC540To24V_101" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">21</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO21</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="22 BatteryChargerDCDC540Vto24V" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">22</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO22</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="23 NC_156" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">23</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO23</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="24 NC_157" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">24</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO24</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="25 NC_158" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">25</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO25</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="26 NC_159" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">26</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO26</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="27 NC_160" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">27</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO27</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="28 NC_161" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">28</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO28</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="29 NC_162" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">29</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO29</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="30 NC_163" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">30</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO30</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
				<Item Name="31 NC_164" Type="Variable">
					<Property Name="featurePacks" Type="Str">Industrial</Property>
					<Property Name="Industrial:BufferingEnabled" Type="Str">False</Property>
					<Property Name="Industrial:ChannelIndex" Type="Str">31</Property>
					<Property Name="Industrial:IODirection" Type="Str">Output</Property>
					<Property Name="Industrial:IsNetworkPublished" Type="Str">True</Property>
					<Property Name="Industrial:Mode" Type="Str">1</Property>
					<Property Name="Industrial:PhysicalName" Type="Str">DO31</Property>
					<Property Name="Network:UseBinding" Type="Str">False</Property>
					<Property Name="Network:UseBuffering" Type="Str">False</Property>
					<Property Name="numTypedefs" Type="UInt">0</Property>
					<Property Name="type" Type="Str">Industrial</Property>
					<Property Name="typeDesc" Type="Bin">&amp;1!!!"1!A!!!!!!"!!1!)1!"!!!!!!!!!!</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="ProjectApplication.lvclass" Type="LVClass" URL="../ProjectApplication/ProjectApplication.lvclass"/>
		<Item Name="Main.vi" Type="VI" URL="../ProjectApplication/Main.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Compare Two Paths.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Compare Two Paths.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="NI_Matrix.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/Matrix/NI_Matrix.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Create Directory Recursive.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Create Directory Recursive.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="Check for Equality.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Check for Equality.vi"/>
				<Item Name="DWDT Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Error Code.vi"/>
				<Item Name="WDT Append Waveforms DBL.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms DBL.vi"/>
				<Item Name="WDT Append Waveforms I64.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I64.vi"/>
				<Item Name="WDT Append Waveforms I32.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I32.vi"/>
				<Item Name="WDT Append Waveforms I16.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms I16.vi"/>
				<Item Name="WDT Append Waveforms EXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms EXT.vi"/>
				<Item Name="WDT Append Waveforms CXT.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CXT.vi"/>
				<Item Name="WDT Append Waveforms CDB.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/WDT Append Waveforms CDB.vi"/>
				<Item Name="Append Waveforms.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/WDTOps.llb/Append Waveforms.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DTbl Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital to Boolean Array.vi"/>
				<Item Name="DWDT Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Digital to Boolean Array.vi"/>
				<Item Name="Digital to Boolean Array.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Digital to Boolean Array.vi"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Analog to Digital.vi"/>
				<Item Name="DWDT Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Analog to Digital.vi"/>
				<Item Name="Analog to Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Analog to Digital.vi"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_VariableUtilities.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Variable/NI_VariableUtilities.lvlib"/>
				<Item Name="ni_tagger_lv_FlushAllConnections.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_FlushAllConnections.vi"/>
			</Item>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="FpgaRef.ctl" Type="VI" URL="../Targets/cRIO9114/FpgaRef.ctl"/>
			<Item Name="IoEngineApplication.lvclass" Type="LVClass" URL="../../../labqt/main/Application/IoEngineApplication/IoEngineApplication.lvclass"/>
			<Item Name="OutputStream.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/OutputStream/OutputStream.lvclass"/>
			<Item Name="Calibrate.lvclass" Type="LVClass" URL="../../../labqt/main/Calibration/Calibrate/Calibrate.lvclass"/>
			<Item Name="Threading.lvclass" Type="LVClass" URL="../../../labqt/main/Execution/Threading/Threading.lvclass"/>
			<Item Name="ExecutionBase.lvclass" Type="LVClass" URL="../../../labqt/main/Execution/ExecutionBase/ExecutionBase.lvclass"/>
			<Item Name="IO.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/IO/IO.lvclass"/>
			<Item Name="InputStream.lvlib" Type="Library" URL="../../../labqt/main/IOStreams/InputStream/InputStream.lvlib"/>
			<Item Name="IoHandler.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/IOHandler/IoHandler.lvclass"/>
			<Item Name="DeviceWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/DeviceWriter/DeviceWriter.lvclass"/>
			<Item Name="DeviceReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/DeviceReader/DeviceReader.lvclass"/>
			<Item Name="ApplicationBase.lvclass" Type="LVClass" URL="../../../labqt/main/Application/ApplicationBase/ApplicationBase.lvclass"/>
			<Item Name="LinearCalibration.lvclass" Type="LVClass" URL="../../../labqt/main/Calibration/Concrete/LinearCalibration/LinearCalibration.lvclass"/>
			<Item Name="FunctionCalibration.lvclass" Type="LVClass" URL="../../../labqt/main/Calibration/FunctionCalibration/FunctionCalibration.lvclass"/>
			<Item Name="CalculateKandM.vi" Type="VI" URL="../../../labqt/main/Utilities/Calculation/CalculateKandM.vi"/>
			<Item Name="WaitForOpen.vi" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/WaitForOpen.vi"/>
			<Item Name="UserEventType.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/UserEventType.ctl"/>
			<Item Name="SubPanelOutgoingData.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/SubPanelOutgoingData.ctl"/>
			<Item Name="OutgoingUserEvent.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/OutgoingUserEvent.ctl"/>
			<Item Name="BroadcastEventType.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/BroadcastEventType.ctl"/>
			<Item Name="BroadCastIncomingEvent.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/BroadCastIncomingEvent.ctl"/>
			<Item Name="SubPanelIncomingData.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/SubPanelIncomingData.ctl"/>
			<Item Name="IncomingUserEvent.ctl" Type="VI" URL="../../../labqt/main/GUI/SubPanelUtilities/IncomingUserEvent.ctl"/>
			<Item Name="OutputBufferedDeviceReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/OutputBufferedDeviceReader/OutputBufferedDeviceReader.lvclass"/>
			<Item Name="RingBuffer.lvlib" Type="Library" URL="../../../labqt/main/Buffer/RingBuffer/RingBuffer.lvlib"/>
			<Item Name="UnbufferedDeviceReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/UnbufferedDeviceReader/UnbufferedDeviceReader.lvclass"/>
			<Item Name="UserDefinedErrorReasons.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/UserDefinedErrorReasons/UserDefinedErrorReasons.lvclass"/>
			<Item Name="LogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/LogWriter/LogWriter.lvclass"/>
			<Item Name="Logger.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/Logger/Logger.lvclass"/>
			<Item Name="Stream.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/Stream/Stream.lvclass"/>
			<Item Name="FileStream.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/FileStream/FileStream.lvclass"/>
			<Item Name="FileWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/FileWriter/FileWriter.lvclass"/>
			<Item Name="FileReader.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/FileReader/FileReader.lvclass"/>
			<Item Name="BusyDialog.vi" Type="VI" URL="../../../labqt/main/Utilities/Dialog/BusyDialog/BusyDialog.vi"/>
			<Item Name="BusyDialogState.ctl" Type="VI" URL="../../../labqt/main/Utilities/Dialog/BusyDialog/BusyDialogState.ctl"/>
			<Item Name="SetBusyDialog.vi" Type="VI" URL="../../../labqt/main/Utilities/Dialog/BusyDialog/SetBusyDialog.vi"/>
			<Item Name="HtmlFileLogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/HtmlFileLogWriter/HtmlFileLogWriter.lvclass"/>
			<Item Name="FileLogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/FileLogWriter/FileLogWriter.lvclass"/>
			<Item Name="TextFileLogWriter.lvclass" Type="LVClass" URL="../../../labqt/main/Logging/TextFileLogWriter/TextFileLogWriter.lvclass"/>
			<Item Name="SimulatedBufferedDeviceReader.lvclass" Type="LVClass" URL="../../../labqt/drivers/DAQ/SimulatedBufferedDeviceReader/SimulatedBufferedDeviceReader.lvclass"/>
			<Item Name="CRioBufferedDeviceReader.lvclass" Type="LVClass" URL="../../../labqt/drivers/DAQ/CRioBufferedDeviceReader/CRioBufferedDeviceReader.lvclass"/>
			<Item Name="UnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../../labqt/main/IOStreams/UnbufferedDeviceWriter/UnbufferedDeviceWriter.lvclass"/>
			<Item Name="SimulatedUnbufferedDeviceWriter.lvclass" Type="LVClass" URL="../../../labqt/drivers/DAQ/SimulatedUnbufferedDeviceWriter/SimulatedUnbufferedDeviceWriter.lvclass"/>
			<Item Name="CRioCSeriesDeviceWriter.lvclass" Type="LVClass" URL="../../../labqt/drivers/DAQ/CRioCSeriesDeviceWriter/CRioCSeriesDeviceWriter.lvclass"/>
			<Item Name="DutRange.ctl" Type="VI" URL="../Misc/DutRange.ctl"/>
			<Item Name="Main.lvbitx" Type="Document" URL="../FPGA Bitfiles/Main.lvbitx"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="CRioCSeriesDeviceReader.lvclass" Type="LVClass" URL="../../../labqt/drivers/DAQ/CRioCSeriesDeviceReader/CRioCSeriesDeviceReader.lvclass"/>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="ControlSystemRTApp" Type="{69A947D5-514E-4E75-818E-69657C0547D8}">
				<Property Name="App_INI_aliasGUID" Type="Str">{B6CBE163-3CD1-4C4F-9952-90937A00BF23}</Property>
				<Property Name="App_INI_GUID" Type="Str">{53D4ED2A-ECE4-4607-B0CB-BB9335E00487}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{AD5B6792-2842-4421-BC13-16B62AF3894A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">ControlSystemRTApp</Property>
				<Property Name="Bld_compilerOptLevel" Type="Int">0</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../Builds/RT</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{4BD9FFAD-F79D-4AA3-ADCF-F3728098975B}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/c/ni-rt/startup</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">startup.rtexe</Property>
				<Property Name="Destination[0].path" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/c/ni-rt/startup/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{E2871321-C1B2-43F9-9FA5-2292D5C5D724}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/ControlSystem/InputVariables</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/ControlSystem/OutputVariables</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/ControlSystem/VariablesSequence</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/ControlSystem/Main.vi</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[4].type" Type="Str">VI</Property>
				<Property Name="Source[5].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/ControlSystem/ProjectApplication.lvclass/SupportFiles</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">6</Property>
				<Property Name="TgtF_fileDescription" Type="Str">My Real-Time Application</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">My Real-Time Application</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 </Property>
				<Property Name="TgtF_productName" Type="Str">My Real-Time Application</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{EB325DDF-6DD4-4FA2-A10D-36A594455CAE}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">startup.rtexe</Property>
			</Item>
		</Item>
	</Item>
</Project>
